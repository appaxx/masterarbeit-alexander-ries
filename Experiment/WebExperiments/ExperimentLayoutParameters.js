"use strict";

function ExperimentLayoutParameters(elementSpaceWith, elementSpaceHeight, numberOfElements, pageWidth) {
    this.elementSpaceWith = elementSpaceWith;
    this.elementSpaceHeight = elementSpaceHeight;
    this.numberOfElements = numberOfElements;
    this.pageWidth = pageWidth;
}