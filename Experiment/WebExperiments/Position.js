"use strict";

function Position(x, y) {
    var xCoordinate = x;
    var yCoordinate = y;

    this.getX = function() {
        return xCoordinate;
    }

    this.getY = function() {
        return yCoordinate;
    }
}