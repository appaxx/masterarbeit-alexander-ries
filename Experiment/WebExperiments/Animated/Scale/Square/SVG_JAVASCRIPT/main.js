var positionsList;
var elementWidth = 10;
var elementHeight = 10;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;
var elements;

function setupExperiment() {

    var parameters = new ExperimentLayoutParameters(45, 45, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);

    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());

        var squareElement = document.createElementNS("http://www.w3.org/2000/svg",'rect');
        squareElement.setAttribute('class', "box");
        squareElement.setAttribute('x', position.getX());
        squareElement.setAttribute('y', position.getY());
        squareElement.setAttribute('width',elementWidth);
        squareElement.setAttribute('height',elementHeight);
        squareElement.setAttribute('fill',"blue");
        svg.appendChild(squareElement);
    }
    elements = document.getElementsByClassName("box");
}

function animateOpacity(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var scale = 4 * (time / durationInMilliseconds);
    if (scale <= 4) {
        for (var i = 0; i < elements.length; i++) {
            var element = elements.item(i);
            element.style.transform = "scale("+scale+")";
            element.style.transformOrigin = "50% 50%";
        }
        requestAnimFrame(function () {
            animateOpacity(startTime, durationInMilliseconds);
        });
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animateOpacity(startTime, 600);
    }, 1000);
}