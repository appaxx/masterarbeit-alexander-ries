
function setupExperiment() {
    var elementsNumberParameterString = "elementsNumber";
    var elementWidth = 5;
    var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var parameters = new ExperimentLayoutParameters(45, 45, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    var positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    var svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);

    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        var circleElement = document.createElementNS("http://www.w3.org/2000/svg",'circle');
        circleElement.setAttribute('class',"square");
        circleElement.setAttribute('cx', position.getX());
        circleElement.setAttribute('cy', position.getY());
        circleElement.setAttribute('r',elementWidth);
        circleElement.setAttribute('fill',"blue");
        circleElement.setAttribute('stroke',"blue");
        svg.appendChild(circleElement);
    }
}

function startAnimation() {
    $(".square").each(function() {
        console.log("Changing fill opacity");
        $(this).css("transform" , " scale(4)");
        $(this).css("transform-origin", "50% 50%");
    });
}