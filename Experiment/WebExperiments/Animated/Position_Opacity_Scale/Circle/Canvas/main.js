var positionsList;
var elementWidth = 5;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;

function setupExperiment() {
    var parameters = new ExperimentLayoutParameters(45, 45, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var canvasContainer = document.getElementById("canvasContainer");
    canvasContainer.width = pageWidth;
    canvasContainer.height = pageHeight;
    ctx = canvasContainer.getContext("2d");
    drawCirclesWithWithYValueOpacityAndScale(0, 0, 1);
}

function drawCirclesWithWithYValueOpacityAndScale(yValue, opacity, scale) {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        ctx.beginPath();
        ctx.arc(position.getX(), position.getY()+yValue, elementWidth*scale, 0, 2 * Math.PI, false);
        ctx.fillStyle = "rgba(0, 0, 255, "+opacity+")";
        ctx.fill();
    }
}

function animatePositionOpacityAndScale(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newY = 50 * (time / durationInMilliseconds);
    var newOpacity = (time / durationInMilliseconds);
    var newScale = 1+ (3 * (time / durationInMilliseconds));
    if (newOpacity <= 1) {
        ctx.clearRect(0, 0, pageWidth, pageHeight);
        drawCirclesWithWithYValueOpacityAndScale(newY, newOpacity, newScale);
        requestAnimFrame(function () {
            animatePositionOpacityAndScale(startTime, durationInMilliseconds);
        });
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animatePositionOpacityAndScale(startTime, 600);
    }, 1000);
}