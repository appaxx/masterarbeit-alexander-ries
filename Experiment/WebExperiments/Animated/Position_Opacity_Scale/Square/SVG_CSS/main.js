
function setupExperiment() {
    var elementsNumberParameterString = "elementsNumber";
    var elementWidth = 10;
    var elementHeight = 10;
    var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var parameters = new ExperimentLayoutParameters(45, 45, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    var positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    var svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);

    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        var squareElement = document.createElementNS("http://www.w3.org/2000/svg",'rect');
        squareElement.setAttribute('class',"box");
        squareElement.setAttribute('x', position.getX());
        squareElement.setAttribute('y', position.getY());
        squareElement.setAttribute('width',elementWidth);
        squareElement.setAttribute('height',elementHeight);
        squareElement.setAttribute('fill',"blue");
        svg.appendChild(squareElement);
    }
}

function startAnimation() {
    $(".box").each(function() {
        $(this).css("transform", "translateY(50px) scale(4)");
        $(this).css("opacity", "1");
    });
}