
var positionsList;
var elementWidth = 2;
var elementHeight = 10;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;

function setupExperiment() {
    var parameters = new ExperimentLayoutParameters(5, 55, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var canvasContainer = document.getElementById("canvasContainer");
    canvasContainer.width = pageWidth;
    canvasContainer.height = pageHeight;
    ctx = canvasContainer.getContext("2d");
    drawDrawLinesWithWithLenght(elementHeight);
}

function animatePosition(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newLenght = 10+40*(time / durationInMilliseconds);
    if (newLenght <= 50) {
        ctx.clearRect(0, 0, pageWidth, pageHeight);
        drawDrawLinesWithWithLenght(newLenght);
        requestAnimFrame(function () {
            animatePosition(startTime, durationInMilliseconds);
        });
    }
}

function drawDrawLinesWithWithLenght(lenght) {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        ctx.beginPath();
        ctx.moveTo(position.getX(), position.getY());
        ctx.lineTo(position.getX(), position.getY()+lenght);
        ctx.lineWidth = elementWidth;
        ctx.strokeStyle = 'blue';
        ctx.stroke();
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animatePosition(startTime, 600);
    }, 1000);
}