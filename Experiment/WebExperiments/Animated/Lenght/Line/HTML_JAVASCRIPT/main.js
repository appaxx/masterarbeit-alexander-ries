var positionsList;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;
var elements;

function setupExperiment() {

    var parameters = new ExperimentLayoutParameters(5, 55, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        $("<div>")
            .addClass("line")
            .css("top", position.getY() + "px")
            .css("left", position.getX() + "px")
            .appendTo("body");
    }
    elements = document.getElementsByClassName("line");
}

function animateTransform(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newLenght = 10+40 * (time / durationInMilliseconds);
    if (newLenght <= 50) {
        for (var i = 0; i < elements.length; i++) {
            var element = elements.item(i);
            element.style.height = newLenght+"px";
        }
        requestAnimFrame(function () {
            animateTransform(startTime, durationInMilliseconds);
        });
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animateTransform(startTime, 600);
    }, 1000);
}