var positionsList;
var elementWidth = 20;
var elementHeight = 20;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;

function setupExperiment() {

    var parameters = new ExperimentLayoutParameters(30, 30, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var canvasContainer = document.getElementById("canvasContainer");
    canvasContainer.width = pageWidth;
    canvasContainer.height = pageHeight;
    ctx = canvasContainer.getContext("2d");
    drawSquaresCirclesAndLinesWithYValue(0);
}

function animateOpacity(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newYOffset = 50 * (time / durationInMilliseconds);
    if (newYOffset <= 50) {
        ctx.clearRect(0, 0, pageWidth, pageHeight);
        drawSquaresCirclesAndLinesWithYValue(newYOffset);
        requestAnimFrame(function () {
            animateOpacity(startTime, durationInMilliseconds);
        });
    }
}

function drawSquaresCirclesAndLinesWithYValue(yValue) {
    for (var i = 0; i < elementsNumber-2; i=i+3) {
        var squarePosition = positionsList[i];
        var circlePosition = positionsList[i+1];
        var linePosition = positionsList[i+2];
        //Square
        ctx.beginPath();
        ctx.rect(squarePosition.getX(), squarePosition.getY()+yValue, elementWidth, elementHeight);
        ctx.fillStyle = "rgba(0, 0, 255, 1)";
        ctx.fill();
        //Circle
        ctx.beginPath();
        ctx.arc(circlePosition.getX()+elementWidth/2, circlePosition.getY()+elementWidth/2+yValue, elementWidth/2, 0, 2 * Math.PI, false);
        ctx.fillStyle = "rgba(0, 0, 255, 1)";
        ctx.fill();
        //Line
        ctx.beginPath();
        ctx.moveTo(linePosition.getX(), linePosition.getY()+yValue);
        ctx.lineTo(linePosition.getX(), linePosition.getY()+elementHeight+yValue);
        ctx.lineWidth = elementWidth/10;
        ctx.strokeStyle = "rgba(0, 0, 255, 1)";
        ctx.stroke();
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animateOpacity(startTime, 600);
    }, 1000);
}