var positionsList;
var elementWidth = 10;
var elementHeight = 10;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;

function setupExperiment() {
    var parameters = new ExperimentLayoutParameters(15, 15, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var canvasContainer = document.getElementById("canvasContainer");
    canvasContainer.width = pageWidth;
    canvasContainer.height = pageHeight;
    ctx = canvasContainer.getContext("2d");
    drawSquaresWithWithYValue(0);
}

function animatePosition(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newY = 50*(time / durationInMilliseconds);
    if (newY <= 50) {
        ctx.clearRect(0, 0, pageWidth, pageHeight);
        drawSquaresWithWithYValue(newY);
        requestAnimFrame(function () {
            animatePosition(startTime, durationInMilliseconds);
        });
    }
}

function drawSquaresWithWithYValue(yValue) {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        ctx.beginPath();
        ctx.rect(position.getX(), position.getY()+yValue, elementWidth, elementHeight);
        ctx.fillStyle = "blue";
        ctx.fill();
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animatePosition(startTime, 600);
    }, 1000);
}