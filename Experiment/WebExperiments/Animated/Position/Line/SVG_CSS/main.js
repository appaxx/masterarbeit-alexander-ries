
function setupExperiment() {
    var elementsNumberParameterString = "elementsNumber";
    var elementWidth = 2;
    var elementHeight = 50;
    var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var parameters = new ExperimentLayoutParameters(5, 55, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    var positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    var svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);

    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        var lineElement = document.createElementNS("http://www.w3.org/2000/svg",'line');
        lineElement.setAttribute('class',"line");
        lineElement.setAttribute('x1', position.getX());
        lineElement.setAttribute('y1', position.getY());
        lineElement.setAttribute('x2', position.getX());
        lineElement.setAttribute('y2', position.getY()+elementHeight);
        lineElement.setAttribute('stroke-width',elementWidth);
        lineElement.setAttribute('stroke',"blue");
        svg.appendChild(lineElement);
    }
}

function startAnimation() {
    $(".line").each(function() {
        console.log("Changing fill opacity");
        $(this).css("transform", "translateY(50px)");
    });
}