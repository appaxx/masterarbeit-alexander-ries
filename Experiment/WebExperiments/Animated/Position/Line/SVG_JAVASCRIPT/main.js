var positionsList;
var elementWidth = 2;
var elementHeight = 50;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;
var elements;

function setupExperiment() {

    var parameters = new ExperimentLayoutParameters(5, 55, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);

    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        var lineElement = document.createElementNS("http://www.w3.org/2000/svg",'line');
        lineElement.setAttribute('class',"line");
        lineElement.setAttribute('x1', position.getX());
        lineElement.setAttribute('y1', position.getY());
        lineElement.setAttribute('x2', position.getX());
        lineElement.setAttribute('y2', position.getY()+elementHeight);
        lineElement.setAttribute('stroke-width',elementWidth);
        lineElement.setAttribute('stroke',"blue");
        svg.appendChild(lineElement);
    }
    elements = document.getElementsByClassName("line");
}

function animatePosition(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newY = 50 * (time / durationInMilliseconds);
    if (newY <= 50) {
        for (var i = 0; i < elements.length; i++) {
            var element = elements.item(i);
            element.style.transform = "translateY("+newY+"px)";
        }
        requestAnimFrame(function () {
            animatePosition(startTime, durationInMilliseconds);
        });
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animatePosition(startTime, 600);
    }, 1000);
}