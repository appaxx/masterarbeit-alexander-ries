var positionsList;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var elements;

function setupExperiment() {
    var parameters = new ExperimentLayoutParameters(15, 55, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        $("<div>")
            .addClass("square")
            .css("top", position.getY() + "px")
            .css("left", position.getX() + "px")
            .appendTo("body");
    }
    elements = document.getElementsByClassName("square");
}

function startAnimation() {
    for (var i = 0; i < elements.length; i++) {
        var element = elements.item(i);
        element.style.height = "50px";
    }
}