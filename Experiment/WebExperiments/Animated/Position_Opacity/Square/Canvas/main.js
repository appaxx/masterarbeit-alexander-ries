var positionsList;
var elementWidth = 20;
var elementHeight = 20;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;

function setupExperiment() {
    var parameters = new ExperimentLayoutParameters(25, 25, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var canvasContainer = document.getElementById("canvasContainer");
    canvasContainer.width = pageWidth;
    canvasContainer.height = pageHeight;
    ctx = canvasContainer.getContext("2d");
    drawSquaresWithWithYValueAndOpacity(0, 1);
}

function animatePositionAndOpacity(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newY = 50 * (time / durationInMilliseconds);
    var newOpacity = 1 - (time / durationInMilliseconds);
    if (newOpacity > -0.5) {
        ctx.clearRect(0, 0, pageWidth, pageHeight);
        drawSquaresWithWithYValueAndOpacity(newY, newOpacity);
        requestAnimFrame(function () {
            animatePositionAndOpacity(startTime, durationInMilliseconds);
        });
    }
}

function drawSquaresWithWithYValueAndOpacity(yValue, opacity) {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        ctx.beginPath();
        ctx.rect(position.getX(), position.getY()+yValue, elementWidth, elementHeight);
        ctx.fillStyle = "rgba(0, 0, 255, "+opacity+")"
        ctx.fill();
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animatePositionAndOpacity(startTime, 600);
    }, 1000);
}