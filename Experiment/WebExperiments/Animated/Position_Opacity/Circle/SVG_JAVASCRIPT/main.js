var positionsList;
var elementWidth = 10;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;
var elements;

function setupExperiment() {

    var parameters = new ExperimentLayoutParameters(25, 25, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);

    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        var circleElement = document.createElementNS("http://www.w3.org/2000/svg",'circle');
        circleElement.setAttribute('class', "box");
        circleElement.setAttribute('cx', position.getX());
        circleElement.setAttribute('cy', position.getY());
        circleElement.setAttribute('r',elementWidth);
        circleElement.setAttribute('fill',"blue");
        svg.appendChild(circleElement);
    }
    elements = document.getElementsByClassName("box");
}

function animateOpacity(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newY = 50 * (time / durationInMilliseconds);
    var newOpacity = 1 - (time / durationInMilliseconds);
    if (newOpacity > -0.5) {
        for (var i = 0; i < elements.length; i++) {
            var element = elements.item(i);
            element.style.transform = "translateY("+newY+"px)";
            element.style.opacity = newOpacity+"";
        }
        requestAnimFrame(function () {
            animateOpacity(startTime, durationInMilliseconds);
        });
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animateOpacity(startTime, 600);
    }, 1000);
}