var positionsList;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var elements;

function setupExperiment() {

    var parameters = new ExperimentLayoutParameters(30, 30, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    for (var i = 0; i < elementsNumber-2; i = i+3) {
        var squarePosition = positionsList[i];
        var circlePosition = positionsList[i+1];
        var linePosition = positionsList[i+2];
        $("<div>")
            .addClass("graphic_primitive")
            .addClass("square")
            .css("top", squarePosition.getY() + "px")
            .css("left", squarePosition.getX() + "px")
            .appendTo("body");
        $("<div>")
            .addClass("graphic_primitive")
            .addClass("circle")
            .css("top", circlePosition.getY() + "px")
            .css("left", circlePosition.getX() + "px")
            .appendTo("body");
        $("<div>")
            .addClass("graphic_primitive")
            .addClass("line")
            .css("top", linePosition.getY() + "px")
            .css("left", linePosition.getX() + "px")
            .appendTo("body");
    }
    elements = document.getElementsByClassName("graphic_primitive");
}

function startAnimation() {
    for (var i = 0; i < elements.length; i++) {
        var element = elements.item(i);
        element.style.opacity = 0.0;
    }
}