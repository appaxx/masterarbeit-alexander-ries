var positionsList;
var elementWidth = 20;
var elementHeight = 20;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;
var elements;

function setupExperiment() {

    var parameters = new ExperimentLayoutParameters(30, 30, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);

    for (var i = 0; i < elementsNumber-2; i = i+3) {
        var squarePosition = positionsList[i];
        var circlePosition = positionsList[i+1];
        var linePosition = positionsList[i+2];
        var squareElement = document.createElementNS("http://www.w3.org/2000/svg",'rect');
        squareElement.setAttribute('class','graphic_primitive');
        squareElement.setAttribute('x', squarePosition.getX());
        squareElement.setAttribute('y', squarePosition.getY());
        squareElement.setAttribute('width',elementWidth);
        squareElement.setAttribute('height',elementHeight);
        squareElement.setAttribute('fill',"blue");
        svg.appendChild(squareElement);

        var circleElement = document.createElementNS("http://www.w3.org/2000/svg",'circle');
        circleElement.setAttribute('class',"graphic_primitive");
        circleElement.setAttribute('cx', circlePosition.getX()+elementWidth/2);
        circleElement.setAttribute('cy', circlePosition.getY()+elementWidth/2);
        circleElement.setAttribute('r',elementWidth/2);
        circleElement.setAttribute('fill',"blue");
        circleElement.setAttribute('stroke',"blue");
        svg.appendChild(circleElement);

        var lineElement = document.createElementNS("http://www.w3.org/2000/svg",'line');
        lineElement.setAttribute('class',"graphic_primitive");
        lineElement.setAttribute('x1', linePosition.getX());
        lineElement.setAttribute('y1', linePosition.getY());
        lineElement.setAttribute('x2', linePosition.getX());
        lineElement.setAttribute('y2', linePosition.getY()+elementHeight);
        lineElement.setAttribute('stroke-width',elementWidth/10);
        lineElement.setAttribute('stroke',"blue");
        svg.appendChild(lineElement);
    }
    elements = document.getElementsByClassName("graphic_primitive");
}

function animateOpacity(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newOpacity = 1 - (time / durationInMilliseconds);
    if (newOpacity >= -0.5) {
        for (var i = 0; i < elements.length; i++) {
            var element = elements.item(i);
            element.style.opacity = newOpacity;
        }
        requestAnimFrame(function () {
            animateOpacity(startTime, durationInMilliseconds);
        });
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animateOpacity(startTime, 600);
    }, 1000);
}