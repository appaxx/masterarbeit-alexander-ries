var positionsList;
var elementWidth = 20;
var elementHeight = 20;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;

function setupExperiment() {

    var parameters = new ExperimentLayoutParameters(30, 30, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var canvasContainer = document.getElementById("canvasContainer");
    canvasContainer.width = pageWidth;
    canvasContainer.height = pageHeight;
    ctx = canvasContainer.getContext("2d");
    drawSquaresCirclesAndLinesWithOpacity(1.0, ctx);
}

function animateOpacity(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newOpacity = 1 - (time / durationInMilliseconds);
    if (newOpacity >= -0.5) {
        ctx.clearRect(0, 0, pageWidth, pageHeight);
        drawSquaresCirclesAndLinesWithOpacity(newOpacity);
        requestAnimFrame(function () {
            animateOpacity(startTime, durationInMilliseconds);
        });
    }
}

function drawSquaresCirclesAndLinesWithOpacity(opacity) {
    for (var i = 0; i < elementsNumber-2; i=i+3) {
        var squarePosition = positionsList[i];
        var circlePosition = positionsList[i+1];
        var linePosition = positionsList[i+2];
        //Square
        ctx.beginPath();
        ctx.rect(squarePosition.getX(), squarePosition.getY(), elementWidth, elementHeight);
        ctx.fillStyle = "rgba(0, 0, 255, " + opacity + ")";
        ctx.fill();
        //Circle
        ctx.beginPath();
        ctx.arc(circlePosition.getX()+elementWidth/2, circlePosition.getY()+elementWidth/2, elementWidth/2, 0, 2 * Math.PI, false);
        ctx.fillStyle = "rgba(0, 0, 255, " + opacity + ")";
        ctx.fill();
        //Line
        ctx.beginPath();
        ctx.moveTo(linePosition.getX(), linePosition.getY());
        ctx.lineTo(linePosition.getX(), linePosition.getY()+elementHeight);
        ctx.lineWidth = elementWidth/10;
        ctx.strokeStyle = "rgba(0, 0, 255, " + opacity + ")";
        ctx.stroke();
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animateOpacity(startTime, 600);
    }, 1000);
}