
function setupExperiment() {
    var elementsNumberParameterString = "elementsNumber";
    var elementWidth = 20;
    var elementHeight = 20;
    var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var parameters = new ExperimentLayoutParameters(30, 30, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    var positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    var svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);

    for (var i = 0; i < elementsNumber-2; i = i+3) {
        var squarePosition = positionsList[i];
        var circlePosition = positionsList[i+1];
        var linePosition = positionsList[i+2];
        var squareElement = document.createElementNS("http://www.w3.org/2000/svg",'rect');
        squareElement.setAttribute('class','graphic_primitive');
        squareElement.setAttribute('x', squarePosition.getX());
        squareElement.setAttribute('y', squarePosition.getY());
        squareElement.setAttribute('width',elementWidth);
        squareElement.setAttribute('height',elementHeight);
        squareElement.setAttribute('fill',"blue");
        svg.appendChild(squareElement);

        var circleElement = document.createElementNS("http://www.w3.org/2000/svg",'circle');
        circleElement.setAttribute('class',"graphic_primitive");
        circleElement.setAttribute('cx', circlePosition.getX()+elementWidth/2);
        circleElement.setAttribute('cy', circlePosition.getY()+elementWidth/2);
        circleElement.setAttribute('r',elementWidth/2);
        circleElement.setAttribute('fill',"blue");
        circleElement.setAttribute('stroke',"blue");
        svg.appendChild(circleElement);

        var lineElement = document.createElementNS("http://www.w3.org/2000/svg",'line');
        lineElement.setAttribute('class',"graphic_primitive");
        lineElement.setAttribute('x1', linePosition.getX());
        lineElement.setAttribute('y1', linePosition.getY());
        lineElement.setAttribute('x2', linePosition.getX());
        lineElement.setAttribute('y2', linePosition.getY()+elementHeight);
        lineElement.setAttribute('stroke-width',elementWidth/10);
        lineElement.setAttribute('stroke',"blue");
        svg.appendChild(lineElement);
    }
}

function startAnimation() {
    $(".graphic_primitive").each(function() {
        console.log("Changing fill opacity");
        $(this).css("opacity", "0.0");
    });
}