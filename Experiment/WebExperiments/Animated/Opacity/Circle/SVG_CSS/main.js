
function setupExperiment() {
    var elementsNumberParameterString = "elementsNumber";
    var elementWith = 10;
    var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var parameters = new ExperimentLayoutParameters(30, 30, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    var positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    var svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);

    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        var circleElement = document.createElementNS("http://www.w3.org/2000/svg",'circle');
        circleElement.setAttribute('class',"box");
        circleElement.setAttribute('cx', position.getX());
        circleElement.setAttribute('cy', position.getY());
        circleElement.setAttribute('r',elementWith);
        circleElement.setAttribute('fill',"blue");
        circleElement.setAttribute('stroke',"blue");
        svg.appendChild(circleElement);
    }
}

function startAnimation() {
    $(".box").each(function() {
        console.log("Changing fill opacity");
        $(this).css("opacity", "0.0");
    });
}