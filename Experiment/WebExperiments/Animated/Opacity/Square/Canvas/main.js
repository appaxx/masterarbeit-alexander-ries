var positionsList;
var elementWidth = 20;
var elementHeight = 20;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;

function setupExperiment() {

    var parameters = new ExperimentLayoutParameters(30, 30, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var canvasContainer = document.getElementById("canvasContainer");
    canvasContainer.width = pageWidth;
    canvasContainer.height = pageHeight;
    ctx = canvasContainer.getContext("2d");
    drawSquaresWithOpacity(1.0, ctx);
}

function animateOpacity(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newOpacity = 1 - (time / durationInMilliseconds);
    if (newOpacity >= -0.5) {
        ctx.clearRect(0, 0, pageWidth, pageHeight);
        drawSquaresWithOpacity(newOpacity);
        requestAnimFrame(function () {
            animateOpacity(startTime, durationInMilliseconds);
        });
    }
}

function drawSquaresWithOpacity(opacity) {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        ctx.beginPath();
        ctx.rect(position.getX(), position.getY(), elementWidth, elementHeight);
        ctx.fillStyle = "rgba(0, 0, 255, " + opacity + ")";
        ctx.fill();
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animateOpacity(startTime, 600);
    }, 1000);
}