var positionsList;
var elementWidth = 2;
var elementHeight = 50;
var elementsNumberParameterString = "elementsNumber";
var elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
var pageWidth = $(window).width();
var pageHeight = $(window).height();
var ctx;

function setupExperiment() {

    var parameters = new ExperimentLayoutParameters(5, 60, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    window.requestAnimFrame = (function (callback) {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    var canvasContainer = document.getElementById("canvasContainer");
    canvasContainer.width = pageWidth;
    canvasContainer.height = pageHeight;
    ctx = canvasContainer.getContext("2d");
    drawLinesWithOpacity(1.0, ctx);
}

function animateOpacity(startTime, durationInMilliseconds) {
    var time = (new Date()).getTime() - startTime;
    var newOpacity = 1 - (time / durationInMilliseconds);
    if (newOpacity >= -0.5) {
        ctx.clearRect(0, 0, pageWidth, pageHeight);
        drawLinesWithOpacity(newOpacity);
        requestAnimFrame(function () {
            animateOpacity(startTime, durationInMilliseconds);
        });
    }
}

function drawLinesWithOpacity(opacity) {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        ctx.beginPath();
        ctx.moveTo(position.getX(), position.getY());
        ctx.lineTo(position.getX(), position.getY()+elementHeight);
        ctx.lineWidth = elementWidth;
        ctx.strokeStyle = "rgba(0, 0, 255, " + opacity + ")";
        ctx.stroke();
    }
}

function startAnimation() {
    setTimeout(function () {
        var startTime = (new Date()).getTime();
        animateOpacity(startTime, 600);
    }, 1000);
}