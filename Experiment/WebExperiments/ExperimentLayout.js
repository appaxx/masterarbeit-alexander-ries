"use strict";

var xPointerStartPosition = 10;
var yPointerStartPosition = 50;

function ExperimentLayout() {

    this.getElementPositionsForExperimentParameters = function (experimentLayoutParameters) {
        var xPointerPosition = xPointerStartPosition;
        var yPointerPosition = yPointerStartPosition;
        var result = [];
        for (var i = 0; i < experimentLayoutParameters.numberOfElements; i++) {
            var pointerPositition = new Position(xPointerPosition, yPointerPosition);
            result.push(pointerPositition);
            if (xPointerPosition > experimentLayoutParameters.pageWidth-xPointerStartPosition*2) {
                xPointerPosition = xPointerStartPosition;
                yPointerPosition = yPointerPosition + experimentLayoutParameters.elementSpaceHeight;
            } else {
                xPointerPosition = xPointerPosition + experimentLayoutParameters.elementSpaceWith;
            }
        }
        return result;
    }
}