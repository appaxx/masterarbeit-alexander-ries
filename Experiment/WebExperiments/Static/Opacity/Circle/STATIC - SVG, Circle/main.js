var elementWidth = 10;
var elementHeight = 10;
var elementsNumber;
var positionsList;
var svg;

function setupExperiment() {
    var elementsNumberParameterString = "elementsNumber";
    elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var parameters = new ExperimentLayoutParameters(12, 12, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);
}

function addGraphicPrimitives() {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        var circleElement = document.createElementNS("http://www.w3.org/2000/svg",'circle');
        circleElement.setAttribute('cx', position.getX());
        circleElement.setAttribute('cy', position.getY());
        circleElement.setAttribute('r',elementWidth/2);
        circleElement.setAttribute('fill',"blue");
        circleElement.setAttribute('stroke',"blue");
        circleElement.setAttribute('fill-opacity', "0.5");
        svg.appendChild(circleElement);
    }
}