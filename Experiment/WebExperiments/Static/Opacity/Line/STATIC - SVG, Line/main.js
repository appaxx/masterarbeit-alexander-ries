var elementWidth = 2;
var elementHeight = 20;
var elementsNumber;
var positionsList;
var svg;

function setupExperiment() {
    var elementsNumberParameterString = "elementsNumber";
    elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var parameters = new ExperimentLayoutParameters(5, 2, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);
}

function addGraphicPrimitives() {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        var circleElement = document.createElementNS("http://www.w3.org/2000/svg",'line');
        circleElement.setAttribute('x1', position.getX());
        circleElement.setAttribute('y1', position.getY());
        circleElement.setAttribute('x2', position.getX());
        circleElement.setAttribute('y2', position.getY()+elementHeight);
        circleElement.setAttribute('stroke-width',elementWidth);
        circleElement.setAttribute('height',elementHeight);
        circleElement.setAttribute('stroke',"blue");
        circleElement.setAttribute('fill-opacity', "0.5");
        svg.appendChild(circleElement);
    }
}
