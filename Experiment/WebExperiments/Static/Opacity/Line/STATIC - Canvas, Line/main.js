var elementWidth = 2;
var elementHeight = 20;
var elementsNumber;
var positionsList;
var ctx;

function setupExperiment() {
    var elementsNumberParameterString = "elementsNumber";
    elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var parameters = new ExperimentLayoutParameters(5, 22, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    var canvasContainer = document.getElementById("canvasContainer");
    canvasContainer.width = pageWidth;
    canvasContainer.height = pageHeight;
    ctx = canvasContainer.getContext("2d");
}

function addGraphicPrimitives() {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        ctx.beginPath();
        ctx.moveTo(position.getX(), position.getY());
        ctx.lineTo(position.getX(), position.getY()+elementHeight);
        ctx.strokeStyle = 'rgba(0,0,255,0.5)';
        ctx.lineWidth = elementWidth;
        ctx.stroke();
    }
}