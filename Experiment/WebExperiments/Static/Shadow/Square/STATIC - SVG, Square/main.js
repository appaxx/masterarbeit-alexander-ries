var elementWidth = 10;
var elementHeight = 10;
var elementsNumber;
var positionsList;
var svg;

function setupExperiment() {
    var elementsNumberParameterString = "elementsNumber";
    elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var parameters = new ExperimentLayoutParameters(12, 12, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);

    svg = document.getElementById("svgContainer");
    svg.setAttribute('width',pageWidth);
    svg.setAttribute('height',pageHeight);
}

function addGraphicPrimitives() {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        var circleElement = document.createElementNS("http://www.w3.org/2000/svg",'rect');
        circleElement.setAttribute('class', "shadow");
        circleElement.setAttribute('x', position.getX());
        circleElement.setAttribute('y', position.getY());
        circleElement.setAttribute('width',elementWidth);
        circleElement.setAttribute('height',elementHeight);
        circleElement.setAttribute('fill',"blue");
        circleElement.setAttribute('filter', "url(#f1)");
        svg.appendChild(circleElement);
    }
}