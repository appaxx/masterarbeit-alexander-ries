var elementWidth = 10;
var elementHeight = 10;
var elementsNumber;
var positionsList;
var ctx;

function setupExperiment() {
    var elementsNumberParameterString = "elementsNumber";
    elementsNumber = URLParameterUtil.getUrlParameter(elementsNumberParameterString);
    var pageWidth = $(window).width();
    var pageHeight = $(window).height();
    var parameters = new ExperimentLayoutParameters(12, 12, elementsNumber, pageWidth);
    var experimentLayout = new ExperimentLayout();
    positionsList = experimentLayout.getElementPositionsForExperimentParameters(parameters);
}

function addGraphicPrimitives() {
    for (var i = 0; i < elementsNumber; i++) {
        var position = positionsList[i];
        console.log(position.getX() + " " + position.getY());
        $("<div>")
            .addClass("circle")
            .css("top", position.getY() + "px")
            .css("left", position.getX() + "px")
            .css("width", elementWidth + "px")
            .css("height", elementHeight + "px")
            .appendTo("body");
    }
}
