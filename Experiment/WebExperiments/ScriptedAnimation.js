"use stric";

function ScriptedAnimation(elements, duration, property, propertySuffix, startValue, endValue) {
    var elements = elements;
    var duration = duration;
    var property = property;
    var propertySuffix = propertySuffix;
    var startValue = startValue;
    var endValue = endValue;

    var lastCallTime = 0;
    var changingValue = 0;
    var requestId = 0;

    this.start = function () {
        lastCallTime = new Date().getTime();
        changingValue = startValue;
        iterate();
    };

    function iterate() {
        now = new Date().getTime();
        var delta = now - lastCallTime;
        lastCallTime = now;
        var distance = changingValue / duration * delta;
        changingValue = changingValue + distance;
        for (var i = 0; i < elements.length; i++) {
            var element = elements.item(i);
            element.style[property] = changingValue + propertySuffix;
        }
        if (changingValue <= endValue) {
            requestId = requestAnimationFrame(iterate);
        } else {
            cancelAnimationFrame(requestId);
        }
    }
}
