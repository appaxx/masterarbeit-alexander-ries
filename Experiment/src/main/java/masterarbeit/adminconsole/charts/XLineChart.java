package masterarbeit.adminconsole.charts;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import masterarbeit.dto.XYSeriesData;

import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;
import org.knowm.xchart.style.Styler.ChartTheme;
import org.knowm.xchart.style.Styler.LegendPosition;
import org.knowm.xchart.style.lines.SeriesLines;

public class XLineChart {
	
	public XLineChart(String header, String yAxisTitle, List<XYSeriesData> xySeriesDataList) {
	    final XYChart chart = new XYChartBuilder().title(header).width(600).height(400).xAxisTitle("Number of Elements").yAxisTitle(yAxisTitle).theme(ChartTheme.GGPlot2).build();
	    chart.getStyler().setLegendPosition(LegendPosition.OutsideE);
	    chart.getStyler().setLegendVisible(false);
	    chart.getStyler().setMarkerSize(0);
	    chart.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Line);
	    for(XYSeriesData xySeriesData : xySeriesDataList) {
	    	XYSeries series = chart.addSeries(xySeriesData.getName(), xySeriesData.getXCoordinates(), xySeriesData.getYCoordinates());
		    if(xySeriesData.getMarker() != null && xySeriesData.getColor() != null && xySeriesData.getColor() != null) {
				series.setMarker(xySeriesData.getMarker());
				series.setLineColor(xySeriesData.getColor());
				series.setLineStyle(SeriesLines.SOLID);
				series.setMarkerColor(xySeriesData.getColor());
		    }
	    }
	    final JFrame frame = new JFrame(header);
	    javax.swing.SwingUtilities.invokeLater(new Runnable() {

		@SuppressWarnings("rawtypes")
		@Override
	      public void run() {
	        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			@SuppressWarnings("unchecked")
			JPanel chartPanel = new XChartPanel(chart);
	        frame.add(chartPanel);
	        frame.pack();
	        frame.setVisible(true);
	      }
	    });
	}
}
