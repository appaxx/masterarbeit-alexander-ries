package masterarbeit.adminconsole.charts;

import java.awt.Color;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;
import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;
import org.knowm.xchart.style.Styler.ChartTheme;
import org.knowm.xchart.style.Styler.LegendPosition;

public class CategoryBarChart {

	public static void createAspectChartWithStringKey(String title, String xAxisTitle, String yAxisTitle, Map<String, Pair<List<String>, List<Double>>> statistics) {
		System.out.println(title+ " - "+xAxisTitle+" - "+yAxisTitle);
		CategoryChart chart = new CategoryChartBuilder().width(800).height(600).title(title).xAxisTitle(xAxisTitle).yAxisTitle(yAxisTitle).theme(ChartTheme.GGPlot2).build();
		for (Entry<String, Pair<List<String>, List<Double>>> entry : statistics.entrySet()) {
			System.out.println(entry.getKey());
			chart.addSeries(entry.getKey(), entry.getValue().getLeft(), entry.getValue().getRight());
			for (int i = 0; i < entry.getValue().getLeft().size(); i++) {
				String leftValue = entry.getValue().getLeft().get(i);
				Double rightValue = entry.getValue().getRight().get(i);
				System.out.println(""+leftValue+":"+rightValue);
			}
		}
		new SwingWrapper<CategoryChart>(chart).displayChart();
	}
	
	public static void createBarChart(String title, String xAxisTitle, String yAxisTitle, Pair<List<String>, List<Double>> statistics) {
		CategoryChart chart = new CategoryChartBuilder().width(800).height(600).title(title).xAxisTitle(yAxisTitle).yAxisTitle(xAxisTitle).theme(ChartTheme.GGPlot2).build();
	    chart.getStyler().setLegendVisible(false);
		chart.addSeries("Animation Smoothness Sum", statistics.getLeft(), statistics.getRight());
	    new SwingWrapper<CategoryChart>(chart).displayChart();
	}
	
	public static void createAspectChartWithDoubleKey(String title, String xAxisTitle, String yAxisTitle, Map<Double, Pair<List<String>, List<Double>>> statistics) {
		System.out.println(title+ " - "+xAxisTitle+" - "+yAxisTitle);
		CategoryChart chart = new CategoryChartBuilder().width(800).height(600).title(title).xAxisTitle(xAxisTitle).yAxisTitle(yAxisTitle).theme(ChartTheme.GGPlot2).build();
		for (Entry<Double, Pair<List<String>, List<Double>>> entry : statistics.entrySet()) {
			System.out.println(entry.getKey());
			chart.addSeries(entry.getKey().toString(), entry.getValue().getLeft(), entry.getValue().getRight());
			for (int i = 0; i < entry.getValue().getLeft().size(); i++) {
				String leftValue = entry.getValue().getLeft().get(i);
				Double rightValue = entry.getValue().getRight().get(i);
				System.out.println(""+leftValue+":"+rightValue);
			}
		}
		chart.getStyler().setSeriesColors(getBlueShades());
		new SwingWrapper<CategoryChart>(chart).displayChart();
	}
	
	private static Color[] getBlueShades() {
		Color[] result = new Color[11];
		result[0] = Color.decode("#00FFFF");
		result[1] = Color.decode("#00E8FF");
		result[2] = Color.decode("#00D1FF");
		result[3] = Color.decode("#00B9FF");
		result[4] = Color.decode("#00A2FF");
		result[5] = Color.decode("#008BFF");
		result[6] = Color.decode("#0074FF");
		result[7] = Color.decode("#005DFF");
		result[8] = Color.decode("#0046FF");
		result[9] = Color.decode("#002EFF");
		result[10] = Color.decode("#0017FF");
		return result;
	}		
	
	public static void createScatterPlot(String header, String xAxisTitle, String yAxisTitle, Map<String, Pair<List<Double>, List<Double>>> statistics) {

		final XYChart chart = new XYChartBuilder().title(header).width(600).height(400).xAxisTitle(xAxisTitle).yAxisTitle(yAxisTitle).theme(ChartTheme.XChart).build();
	    chart.getStyler().setLegendPosition(LegendPosition.OutsideE);
	    chart.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Line);

	    for (Entry<String, Pair<List<Double>, List<Double>>> entry : statistics.entrySet()) {
	    	chart.addSeries(entry.getKey(), entry.getValue().getLeft(), entry.getValue().getRight());
		}
	    
//	    chart.getStyler().setErrorBarsColor(Color.black);

	    // Series
//	    int[] xData = new int[] { 0, 1, 2, 3, 4, 5, 6 };
//	    int[] yData1 = new int[] { 100, 100, 100, 60, 10, 10, 10 };
//	    int[] errdata = new int[] { 50, 20, 10, 52, 9, 2, 1 };
//	    XYSeries series1 = chart.addSeries("Error bar\ntest data", xData, yData1, errdata);
//	    series1.setLineStyle(SeriesLines.SOLID);
//	    series1.setMarker(SeriesMarkers.DIAMOND);
//	    series1.setMarkerColor(Color.GREEN);
		 
	    new SwingWrapper<XYChart>(chart).displayChart();
	}
}
