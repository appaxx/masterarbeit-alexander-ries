package masterarbeit.adminconsole;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import masterarbeit.adminconsole.charts.XLineChart;
import masterarbeit.db.MongoDBConnector;
import masterarbeit.dto.GraphicPrimitive;
import masterarbeit.dto.XYSeriesData;
import masterarbeit.dto.experiment.AnimatedExperiment;
import masterarbeit.dto.experiment.AnimatedProperty;
import masterarbeit.dto.experiment.Property;
import masterarbeit.dto.experiment.StaticExperiment;
import masterarbeit.experiment.ExperimentExecutor;

import org.knowm.xchart.style.markers.Marker;

public class AdminConsole extends JFrame {
	private JList<ExperimentType> experimentsTypes;
	private JList<GraphicPrimitive> graphicPrimitives;
	private JList<AnimatedProperty> animatedProperties;
	private JList<Property> styledProperties;
	private JList<Date> experimentDates;
	private DefaultListModel<GraphicPrimitive> graphicPrimitivesListModel = new DefaultListModel<GraphicPrimitive>();
	private DefaultListModel<AnimatedProperty> animatedPropertiesListModel = new DefaultListModel<AnimatedProperty>();
	private DefaultListModel<Property> styledPropertiesListModel = new DefaultListModel<Property>();
	private DefaultListModel<Date> experimentDatesListModel = new DefaultListModel<Date>();
	private enum ExperimentType {STATIC_EXPERIMENT, ANIMATED_EXPERIMENT};
	private List<AdminConsoleButtonClickListener> adminConsoleButtonClickListeners;
	
	private static final long serialVersionUID = 1L;

	public AdminConsole() {
		super("Admin Console");
		this.setSize(300, 700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		adminConsoleButtonClickListeners = new ArrayList<AdminConsoleButtonClickListener>();
		
		BorderLayout baseLayout = new BorderLayout();
		this.setLayout(baseLayout);
		initButtonPanel();
		
		JPanel contentPanel = new JPanel();
		BorderLayout contentLayout = new BorderLayout();
		contentPanel.setLayout(contentLayout);
		this.add(contentPanel, BorderLayout.CENTER);
		initSelectionPanel(contentPanel);
	}
	
	private synchronized void initButtonPanel() {
		FlowLayout buttonLayout = new FlowLayout();
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(buttonLayout);
		JButton runExperimentButton = new JButton("Run Experiment");
		runExperimentButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ExperimentExecutor experimentExecutor = new ExperimentExecutor();
				experimentExecutor.run();
			}
		});
		buttonPanel.add(runExperimentButton);
		JButton buildChartButton = new JButton("Build Chart");
		buildChartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ExperimentType selectedExperimentType = experimentsTypes.getSelectedValue();
				GraphicPrimitive selectedGraphicPrimitive = graphicPrimitives.getSelectedValue();
				List<XYSeriesData> xySeriesDataList = null;
				String header = null;
				String yAxisName = null;
				switch (selectedExperimentType) {
				case STATIC_EXPERIMENT:
					List<Property> selectedStyledProperties = styledProperties.getSelectedValuesList();
					Date staticSelectedDate = experimentDates.getSelectedValue();
					List<StaticExperiment> staticExperiments = MongoDBConnector.getInstance().queryStaticExperimentsByGraphicPrimitiveAndStyledPropertiesAndDate(selectedGraphicPrimitive, selectedStyledProperties, staticSelectedDate);
					xySeriesDataList = createXYSeriesDataListFromStaticExperiments(staticExperiments);
					header = createHeaderFromStaticExperiment(staticExperiments.get(0));
					yAxisName = "Render time in frames (60 FPS)";
					break;
				case ANIMATED_EXPERIMENT:
					List<AnimatedProperty> selectedAnimatedProperties = animatedProperties.getSelectedValuesList();
					Date animatedSelectedDate = experimentDates.getSelectedValue();
					List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByGraphicPrimitiveAndAnimatedPropertiesAndDate(selectedGraphicPrimitive, selectedAnimatedProperties, animatedSelectedDate);
					xySeriesDataList = createXYSeriesDataListFromAnimatedExperiments(animatedExperiments);
					header = createHeaderFromAnimatedExperiment(animatedExperiments.get(0));
					yAxisName = "Identical frames per 60 frames";
					break;
				default:
					break;
				}
				new XLineChart(header, yAxisName, xySeriesDataList);
			}
		});
		buttonPanel.add(buildChartButton);
		this.add(buttonPanel, BorderLayout.PAGE_START);
	}
	
	private String createHeaderFromStaticExperiment(StaticExperiment animatedExperiment) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(animatedExperiment.getGraphicPrimitive().name()+" - ");
		stringBuilder.append(" Static");
		return stringBuilder.toString();
	}
	
	private String createHeaderFromAnimatedExperiment(AnimatedExperiment animatedExperiment) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(animatedExperiment.getGraphicPrimitive().name()+" - ");
		for(AnimatedProperty animatedProperty : animatedExperiment.getAnimation().getProperties()) {
			stringBuilder.append(animatedProperty.name()+" ");
		}
		stringBuilder.append(" Animation");
		return stringBuilder.toString();
	}

	private List<XYSeriesData> createXYSeriesDataListFromStaticExperiments(List<StaticExperiment> staticExperiments) {
		List<XYSeriesData> result = new ArrayList<XYSeriesData>();
		for (StaticExperiment staticExperiment : staticExperiments) {
			String name = staticExperiment.getWebTechnologyCombination().name() + " + " + staticExperiment.getBrowser().name();
			Color color = staticExperiment.getWebTechnologyCombination().getColor();
			Marker marker = staticExperiment.getBrowser().getMarker();
			int coordinesSize = staticExperiment.getExperimentResults().size();
			double[] xCoordinates = new double[coordinesSize];
			double[] yCoordinates = new double[coordinesSize];
			for (int i = 0; i < coordinesSize; i++) {
				if(staticExperiment.getExperimentResults() != null && staticExperiment.getExperimentResults().get(i) != null) {
					if(staticExperiment.getExperimentResults().get(i).getRenderTimeInFrames() != 0) { 
						xCoordinates[i] = staticExperiment.getExperimentResults().get(i).getNumberOfObjects();
						yCoordinates[i] = staticExperiment.getExperimentResults().get(i).getRenderTimeInFrames();
					}
				}
			}
			XYSeriesData xySeriesData = new XYSeriesData(name, xCoordinates, yCoordinates, color, marker);
			result.add(xySeriesData);
		}
		return result;
	}
	
	private List<XYSeriesData> createXYSeriesDataListFromAnimatedExperiments(List<AnimatedExperiment> animatedExperiments) {
		List<XYSeriesData> result = new ArrayList<XYSeriesData>();
		for(AnimatedExperiment animatedExperiment : animatedExperiments) {
			String name = animatedExperiment.getWebTechnologyCombination().name()+" + "+animatedExperiment.getBrowser().name();
			Color color = animatedExperiment.getWebTechnologyCombination().getColor();
			Marker marker = animatedExperiment.getBrowser().getMarker();
			int coordinesSize = animatedExperiment.getExperimentResults().size();
			double[] xCoordinates = new double[coordinesSize];
			double[] yCoordinates = new double[coordinesSize];
			for(int i = 0; i < coordinesSize; i++) {
				xCoordinates[i] = animatedExperiment.getExperimentResults().get(i).getNumberOfObjects();
				yCoordinates[i] = animatedExperiment.getExperimentResults().get(i).getIdenticalFrameRatePer60Frames();
			}
			XYSeriesData xySeriesData = new XYSeriesData(name, xCoordinates, yCoordinates, color, marker);
			result.add(xySeriesData);
			}
		return result;	
	}

	private void initSelectionPanel(JPanel contentPanel) {
		JPanel selectionPanel = new JPanel();
		GridLayout selectionPanelLayout = new GridLayout(5, 0);
		selectionPanel.setLayout(selectionPanelLayout);
		contentPanel.add(selectionPanel, BorderLayout.WEST);
		
		experimentsTypes = new JList<ExperimentType>(ExperimentType.values());
		graphicPrimitives = new JList<GraphicPrimitive>();
		animatedProperties = new JList<AnimatedProperty>();
		styledProperties = new JList<Property>();
		experimentDates = new JList<Date>();

		experimentsTypes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		graphicPrimitives.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		graphicPrimitives.setModel(graphicPrimitivesListModel);
		animatedProperties.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		animatedProperties.setModel(animatedPropertiesListModel);
		styledProperties.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		styledProperties.setModel(styledPropertiesListModel);
		experimentDates.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		experimentDates.setModel(experimentDatesListModel);

		selectionPanel.add(new JScrollPane(experimentsTypes));
		selectionPanel.add(new JScrollPane(experimentDates));
		selectionPanel.add(new JScrollPane(graphicPrimitives));
		selectionPanel.add(new JScrollPane(animatedProperties));
		selectionPanel.add(new JScrollPane(styledProperties));

		experimentsTypes.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {//This line prevents double events
					System.out.println("Values Changed");
					switch (experimentsTypes.getSelectedValue()) {
					case STATIC_EXPERIMENT:
						List<Date> staticDates = MongoDBConnector.getInstance().queryDatesFromStaticExperiemnts();
						experimentDatesListModel.clear();
						for(Date date : staticDates) {
							experimentDatesListModel.addElement(date);
						}
						break;
					case ANIMATED_EXPERIMENT:
						List<Date> animatedDates = MongoDBConnector.getInstance().queryDatesFromAnimatedExperiemnts();
						experimentDatesListModel.clear();
						for(Date date : animatedDates) {
							experimentDatesListModel.addElement(date);
						}
						break;
					default:
						break;
					}
			    }
			}
		});
		
		experimentDates.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {//This line prevents double events
					System.out.println("Values Changed");
					switch (experimentsTypes.getSelectedValue()) {
					case STATIC_EXPERIMENT:
						Date staticSelectedDate = experimentDates.getSelectedValue();
						if (staticSelectedDate != null) {
							List<GraphicPrimitive> staticGraphicPrimitivesValues = MongoDBConnector.getInstance().queryStaticGraphicPrimitivesForDate(staticSelectedDate);
							graphicPrimitivesListModel.clear();
							for(GraphicPrimitive graphicPrimitive : staticGraphicPrimitivesValues) {
								graphicPrimitivesListModel.addElement(graphicPrimitive);
							}
							List<Property> styledProperties = MongoDBConnector.getInstance().queryStyledPropertiesForDate(staticSelectedDate);
							styledPropertiesListModel.clear();
							for(Property styledProperty : styledProperties) {
								styledPropertiesListModel.addElement(styledProperty);
							}
						}
						break;
					case ANIMATED_EXPERIMENT:
						Date animatedSelectedDate = experimentDates.getSelectedValue();
						if (animatedSelectedDate != null) {
							List<GraphicPrimitive> animatedGraphicPrimitivesValues = MongoDBConnector.getInstance().queryAnimatedGraphicPrimitivesForDate(animatedSelectedDate);
							graphicPrimitivesListModel.clear();
							for(GraphicPrimitive graphicPrimitive : animatedGraphicPrimitivesValues) {
								graphicPrimitivesListModel.addElement(graphicPrimitive);
							}
							List<AnimatedProperty> animatedPropertyValues = MongoDBConnector.getInstance().queryAnimatedPropertiesForDate(animatedSelectedDate);
							animatedPropertiesListModel.clear();
							for(AnimatedProperty animatedProperty : animatedPropertyValues) {
								animatedPropertiesListModel.addElement(animatedProperty);
							}
						}
						break;
					default:
						break;
					}
			    }
			}
		});
			
	}
	
	public void addButtonClickListener(AdminConsoleButtonClickListener adminConsoleButtonClickListener) {
		adminConsoleButtonClickListeners.add(adminConsoleButtonClickListener);
	}
	
	public void removeButtonClickListener(AdminConsoleButtonClickListener adminConsoleButtonClickListener) {
		adminConsoleButtonClickListeners.add(adminConsoleButtonClickListener);
	}
	
	public void notifyButtonClickListenerAboutRunExperimentClick() {
		for(AdminConsoleButtonClickListener adminConsoleButtonClickListener : adminConsoleButtonClickListeners) {
			adminConsoleButtonClickListener.runExperimentButtonClicked();
		}
	}
	
	public interface AdminConsoleButtonClickListener {
		public void runExperimentButtonClicked();
	}
}
