package masterarbeit.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import masterarbeit.dto.Browser;
import masterarbeit.dto.GraphicPrimitive;
import masterarbeit.dto.experiment.AnimatedExperiment;
import masterarbeit.dto.experiment.AnimatedProperty;
import masterarbeit.dto.experiment.Property;
import masterarbeit.dto.experiment.StaticExperiment;

import org.bson.Document;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class MongoDBConnector {

	private static MongoDBConnector instance;
	private static MongoClient mongoClient;
	private static MongoDatabase database;
	private static MongoCollection<Document> staticExperimentCollection;
	private static MongoCollection<Document> animatedExperimentCollection;
	private static Gson gson;
	
	private static final String DATABASE_NAME = "performancedb";
	private static final String STATIC_EXPERIMENT_COLLECTION_NAME = "StaticExperiment";
	private static final String ANIMATED_EXPERIMENT_COLLECTION_NAME = "AnimatedExperiment";
	
	private MongoDBConnector() {
	}

	public static MongoDBConnector getInstance() {
		if (MongoDBConnector.instance == null) {
			MongoDBConnector.instance = new MongoDBConnector();
			mongoClient = new MongoClient( "localhost" );
			database = mongoClient.getDatabase(DATABASE_NAME);
			animatedExperimentCollection = database.getCollection(ANIMATED_EXPERIMENT_COLLECTION_NAME, Document.class);
			staticExperimentCollection = database.getCollection(STATIC_EXPERIMENT_COLLECTION_NAME, Document.class);
			gson = new Gson();
		}
		return MongoDBConnector.instance;
	}
	
	private String convertDateToDBFormattedString(Date date) {
		String result = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy h:mm:ss a");
        result = dateFormat.format(date);
        System.out.println("Formatted date "+result);
        return result;
	}
	
	public void insertStaticExperiment(StaticExperiment staticExperiment) {
		String json = gson.toJson(staticExperiment);    
		Document basicDBObject = Document.parse(json);          
		staticExperimentCollection.insertOne(basicDBObject);
	}
	
	public void insertAnimatedExperiment(AnimatedExperiment animatedExperiemnt) {
		String json = gson.toJson(animatedExperiemnt);    
		Document basicDBObject = Document.parse(json);          
		animatedExperimentCollection.insertOne(basicDBObject);
	}
	
	public List<Date> queryDatesFromStaticExperiemnts() {
		List<Date> result = new ArrayList<Date>();
		MongoCursor<String> iterator = staticExperimentCollection.distinct("date", String.class).iterator();
		while (iterator.hasNext()) {
			String document = iterator.next();
			@SuppressWarnings("deprecation")
			Date date = new Date(document);
			result.add(date);
		}
		return result;
	}
	
	public List<Date> queryDatesFromAnimatedExperiemnts() {
		List<Date> result = new ArrayList<Date>();
		MongoCursor<String> iterator = animatedExperimentCollection.distinct("date", String.class).iterator();
		while (iterator.hasNext()) {
			String document = iterator.next();
			@SuppressWarnings("deprecation")
			Date date = new Date(document);
			result.add(date);
		}
		return result;
	}
	
	public List<GraphicPrimitive> queryStaticGraphicPrimitivesForDate(Date date)  {
		List<GraphicPrimitive> result = new ArrayList<GraphicPrimitive>();
		String formattedDate = convertDateToDBFormattedString(date);
		MongoCursor<String> iterator = staticExperimentCollection.distinct("graphicPrimitive", String.class).filter(new BasicDBObject("date", formattedDate)).iterator();
		while (iterator.hasNext()) {
			result.add(GraphicPrimitive.valueOf(iterator.next()));
		}
		return result;
	}
	
	public List<GraphicPrimitive> queryAnimatedGraphicPrimitivesForDate(Date date)  {
		List<GraphicPrimitive> result = new ArrayList<GraphicPrimitive>();
		String formattedDate = convertDateToDBFormattedString(date);
		MongoCursor<String> iterator = animatedExperimentCollection.distinct("graphicPrimitive", String.class).filter(new BasicDBObject("date", formattedDate)).iterator();
		while (iterator.hasNext()) {
			result.add(GraphicPrimitive.valueOf(iterator.next()));
		}
		return result;
	}
	
	public List<AnimatedProperty> queryAnimatedPropertiesForDate(Date date) {
		List<AnimatedProperty> result = new ArrayList<AnimatedProperty>();
		String formattedDate = convertDateToDBFormattedString(date);
		MongoCursor<String> iterator = animatedExperimentCollection.distinct("animation.properties", String.class).filter(new BasicDBObject("date", formattedDate)).iterator();
		while (iterator.hasNext()) {
			result.add(AnimatedProperty.valueOf(iterator.next()));
		}
		return result;
	}

	public List<Property> queryStyledPropertiesForDate(Date date) {
		List<Property> result = new ArrayList<Property>();
		String formattedDate = convertDateToDBFormattedString(date);
		MongoCursor<String> iterator = staticExperimentCollection.distinct("styleProperties", String.class).filter(new BasicDBObject("date", formattedDate)).iterator();
		while (iterator.hasNext()) {
			result.add(Property.valueOf(iterator.next()));
		}
		return result;
	}
	
	public List<StaticExperiment> queryStaticExperimentsByGraphicPrimitiveAndStyledPropertiesAndDate(GraphicPrimitive graphicPrimitive, List<Property> styledProperties, Date date) {
		List<StaticExperiment> result = new ArrayList<StaticExperiment>();
		String formattedDate = convertDateToDBFormattedString(date);
		MongoCursor<Document> resultsIterator = staticExperimentCollection.find(getGraphicPrimitivesAndStyledPropertiesAndDateQuery(graphicPrimitive, styledProperties, formattedDate)).iterator();
		while (resultsIterator.hasNext()) {
			Document document = resultsIterator.next();
			StaticExperiment experimentResult = gson.fromJson(document.toJson(), StaticExperiment.class);
			result.add(experimentResult);
		}
		return result;
	}
	
	public List<AnimatedExperiment> queryAnimatedExperimentsByGraphicPrimitiveAndAnimatedPropertiesAndDate(GraphicPrimitive graphicPrimitive, List<AnimatedProperty> animatedProperties, Date date) {
		List<AnimatedExperiment> result = new ArrayList<AnimatedExperiment>();
		String formattedDate = convertDateToDBFormattedString(date);
		MongoCursor<Document> resultsIterator = animatedExperimentCollection.find(getGraphicPrimitivesAndAnimatedPropertiesAndDateQuery(graphicPrimitive, animatedProperties, formattedDate)).iterator();
		while (resultsIterator.hasNext()) {
			Document document = resultsIterator.next();
			AnimatedExperiment experimentResult = gson.fromJson(document.toJson(), AnimatedExperiment.class);
			result.add(experimentResult);
		}
		return result;
	}
	
	public List<StaticExperiment> queryStaticExperimentsByDate(String date) {
		List<StaticExperiment> result = new ArrayList<StaticExperiment>();
		MongoCursor<Document> resultsIterator = staticExperimentCollection.find(new BasicDBObject("date", date)).iterator();
		while (resultsIterator.hasNext()) {
			Document document = resultsIterator.next();
			StaticExperiment experimentResult = gson.fromJson(document.toJson(), StaticExperiment.class);
			result.add(experimentResult);
		}
		return result;
	}
	
	public List<AnimatedExperiment> queryAnimatedExperimentsByDate(String date) {
		List<AnimatedExperiment> result = new ArrayList<AnimatedExperiment>();
		MongoCursor<Document> resultsIterator = animatedExperimentCollection.find(new BasicDBObject("date", date)).iterator();
		while (resultsIterator.hasNext()) {
			Document document = resultsIterator.next();
			AnimatedExperiment experimentResult = gson.fromJson(document.toJson(), AnimatedExperiment.class);
			result.add(experimentResult);
		}
		return result;
	}
	
	public List<AnimatedExperiment> queryAnimatedExperimentsByBrowserAndDate(Browser browser, String date) {
		List<AnimatedExperiment> result = new ArrayList<AnimatedExperiment>();
		MongoCursor<Document> resultsIterator = animatedExperimentCollection.find(getBrowserAndDateQuery(browser, date)).iterator();
		while (resultsIterator.hasNext()) {
			Document document = resultsIterator.next();
			AnimatedExperiment experimentResult = gson.fromJson(document.toJson(), AnimatedExperiment.class);
			result.add(experimentResult);
		}
		return result;
	}
	
	private BasicDBObject getBrowserAndDateQuery(Browser browser, String formattedDate) {
		List<BasicDBObject> criteria = new ArrayList<BasicDBObject>();
		criteria.add(new BasicDBObject("browser", browser.name()));
		criteria.add(new BasicDBObject("date", formattedDate));
		BasicDBObject result = new BasicDBObject("$and", criteria);
		return result;
	}
	
	private BasicDBObject getGraphicPrimitivesAndStyledPropertiesAndDateQuery(GraphicPrimitive graphicPrimitive, List<Property> styledProperties, String formattedDate) {
		List<BasicDBObject> criteria = new ArrayList<BasicDBObject>();
		criteria.add(new BasicDBObject("graphicPrimitive", graphicPrimitive.name()));
		criteria.add(new BasicDBObject("date", formattedDate));
		for (Property styledProperty : styledProperties) {
			criteria.add(new BasicDBObject("styleProperties", styledProperty.name()));
		}
		BasicDBObject result = new BasicDBObject("$and", criteria);
		return result;
	}
	
	private BasicDBObject getGraphicPrimitivesAndAnimatedPropertiesAndDateQuery(GraphicPrimitive graphicPrimitive, List<AnimatedProperty> animatedProperties, String formattedDate) {
		List<BasicDBObject> criteria = new ArrayList<BasicDBObject>();
		criteria.add(new BasicDBObject("graphicPrimitive", graphicPrimitive.name()));
		criteria.add(new BasicDBObject("date", formattedDate));
		for (AnimatedProperty animatedProperty : animatedProperties) {
			criteria.add(new BasicDBObject("animation.properties", animatedProperty.name()));
		}
		BasicDBObject result = new BasicDBObject("$and", criteria);
		return result;
	}
}
