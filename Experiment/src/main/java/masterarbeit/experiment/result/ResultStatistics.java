package masterarbeit.experiment.result;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import masterarbeit.db.MongoDBConnector;
import masterarbeit.dto.Browser;
import masterarbeit.dto.XYSeriesData;
import masterarbeit.dto.experiment.AnimatedExperiment;
import masterarbeit.dto.experiment.AnimatedProperty;
import masterarbeit.dto.experiment.Animation;
import masterarbeit.dto.experiment.Property;
import masterarbeit.dto.experiment.StaticExperiment;
import masterarbeit.dto.experiment.result.AnimatedExperimentResult;
import masterarbeit.dto.experiment.result.StaticExperimentResult;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.knowm.xchart.style.markers.Marker;

public class ResultStatistics {
	private List<String> staticGraphicPrimitivesDates;
	private List<String> staticOpacityGraphicPrimitivesDates;
	private List<String> staticShadowGraphicPrimitivesDates;
	private List<String> positionAnimationExperimentDates;
	private List<String> opacityAnimationExperimentDates;
	private List<String> scaleAnimationExperimentDates;
	private List<String> heightAnimationExperimentDates;
	private String lineLenghtAnimationExperimentDate;
	private List<String> positionOpacityAnimationExperimentDates;
	private List<String> positionOpacityScaleAnimationExperimentDates;
	
	private List<String> allStaticExperimentDates;
	private List<String> allAnimatedExperimentDates;
	
	public ResultStatistics() {
		initExperimentDates();
	}
	
	public Map<String, Pair<List<String>, List<Double>>> createStaticGraphicPrimitivesStatistics() {
		Map<String, Pair<List<String>, List<Double>>> result = new HashMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, Double>> preResult = new HashMap<String, Map<String, Double>>();
		for (String date : allStaticExperimentDates) {
			List<StaticExperiment> animatedExperiments = MongoDBConnector.getInstance().queryStaticExperimentsByDate(date);
			for(StaticExperiment staticExperiment : animatedExperiments) {
				for (StaticExperimentResult animatedExperimentResult : staticExperiment.getExperimentResults()) {
					double renderTimeInFrames = animatedExperimentResult.getRenderTimeInFrames();
					Map<String, Double> graphicPrimitivesValues = preResult.get(staticExperiment.getGraphicPrimitive().name());
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, Double>();
						preResult.put(staticExperiment.getGraphicPrimitive().name(), graphicPrimitivesValues);
					}
					String propertyKey = getPropertyKey(staticExperiment.getStyleProperties());
					Double renderTimeInFramesSum = graphicPrimitivesValues.get(propertyKey);
					if (renderTimeInFramesSum == null) {
						graphicPrimitivesValues.put(propertyKey, Double.valueOf(renderTimeInFrames));
					} else {
						double newRenderTimeInFramesSum = renderTimeInFramesSum.doubleValue() + renderTimeInFrames;
						graphicPrimitivesValues.remove(propertyKey);
						graphicPrimitivesValues.put(propertyKey, Double.valueOf(newRenderTimeInFramesSum));
					}
					
				}
			}
		}
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResult.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	public Map<String, Pair<List<String>, List<Double>>> createStaticBrowserStatistics() {
		Map<String, Pair<List<String>, List<Double>>> result = new TreeMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, Double>> preResult = new TreeMap<String, Map<String, Double>>();
		for (String date : allStaticExperimentDates) {
			List<StaticExperiment> animatedExperiments = MongoDBConnector.getInstance().queryStaticExperimentsByDate(date);
			for(StaticExperiment staticExperiment : animatedExperiments) {
				for (StaticExperimentResult animatedExperimentResult : staticExperiment.getExperimentResults()) {
					double renderTimeInFrames = animatedExperimentResult.getRenderTimeInFrames();
					String browser = staticExperiment.getBrowser().name();
					Map<String, Double> graphicPrimitivesValues = preResult.get(browser);
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, Double>();
						preResult.put(browser, graphicPrimitivesValues);
					}
					String propertyKey = getPropertyKey(staticExperiment.getStyleProperties());
					Double renderTimeInFramesSum = graphicPrimitivesValues.get(propertyKey);
					if (renderTimeInFramesSum == null) {
						graphicPrimitivesValues.put(propertyKey, Double.valueOf(renderTimeInFrames));
					} else {
						double newRenderTimeInFramesSum = renderTimeInFramesSum.doubleValue() + renderTimeInFrames;
						graphicPrimitivesValues.remove(propertyKey);
						graphicPrimitivesValues.put(propertyKey, Double.valueOf(newRenderTimeInFramesSum));
					}
					
				}
			}
		}
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResult.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	public Map<String, Pair<List<String>, List<Double>>> createStaticWebTechnologyStatistics() {
		Map<String, Pair<List<String>, List<Double>>> result = new TreeMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, Double>> preResult = new TreeMap<String, Map<String, Double>>();
		for (String date : allStaticExperimentDates) {
			List<StaticExperiment> animatedExperiments = MongoDBConnector.getInstance().queryStaticExperimentsByDate(date);
			for(StaticExperiment staticExperiment : animatedExperiments) {
				for (StaticExperimentResult animatedExperimentResult : staticExperiment.getExperimentResults()) {
					double renderTimeInFrames = animatedExperimentResult.getRenderTimeInFrames();
					String webTechnologyCombination = staticExperiment.getWebTechnologyCombination().name();
					Map<String, Double> graphicPrimitivesValues = preResult.get(webTechnologyCombination);
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, Double>();
						preResult.put(webTechnologyCombination, graphicPrimitivesValues);
					}
					String propertyKey = getPropertyKey(staticExperiment.getStyleProperties());
					Double renderTimeInFramesSum = graphicPrimitivesValues.get(propertyKey);
					if (renderTimeInFramesSum == null) {
						graphicPrimitivesValues.put(propertyKey, Double.valueOf(renderTimeInFrames));
					} else {
						double newRenderTimeInFramesSum = renderTimeInFramesSum.doubleValue() + renderTimeInFrames;
						graphicPrimitivesValues.remove(propertyKey);
						graphicPrimitivesValues.put(propertyKey, Double.valueOf(newRenderTimeInFramesSum));
					}
					
				}
			}
		}
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResult.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	public Map<String, Pair<List<String>, List<Double>>> createAnimatedBrowserStatistics() {
		Map<String, Pair<List<String>, List<Double>>> result = new HashMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, Double>> preResult = new HashMap<String, Map<String, Double>>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					String browser = animatedExperiment.getBrowser().name();
					Map<String, Double> graphicPrimitivesValues = preResult.get(browser);
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, Double>();
						preResult.put(browser, graphicPrimitivesValues);
					}
					String animationKey = getAnimationKey(animatedExperiment.getAnimation());
					Double renderTimeInFramesSum = graphicPrimitivesValues.get(animationKey);
					if (renderTimeInFramesSum == null) {
						graphicPrimitivesValues.put(animationKey, Double.valueOf(animationSmoothness));
					} else {
						double newRenderTimeInFramesSum = renderTimeInFramesSum.doubleValue() + animationSmoothness;
						graphicPrimitivesValues.remove(animationKey);
						graphicPrimitivesValues.put(animationKey, Double.valueOf(newRenderTimeInFramesSum));
					}
					
				}
			}
		}
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResult.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	public Pair<List<String>, List<Double>> createAnimatedAnimationStatistics() {
		Map<String/*Animation*/, List<Double>/*Values*/> animationSmoothnessValuesMap = new HashMap<String, List<Double>>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					double numberOfObjects = animatedExperimentResult.getNumberOfObjects();
					if(numberOfObjects != 1) {
						numberOfObjects--;
					}
					if(numberOfObjects <= 2000 && numberOfObjects != 500 && numberOfObjects != 1500) {
						String animationKey = getAnimationKey(animatedExperiment.getAnimation());
						List<Double> animationSmoothnessValues = animationSmoothnessValuesMap.get(animationKey);
						if (animationSmoothnessValues == null) {
							animationSmoothnessValues = new ArrayList<Double>();
							animationSmoothnessValuesMap.put(animationKey, animationSmoothnessValues);
						}
						animationSmoothnessValues.add(Double.valueOf(animationSmoothness));
					}
					}
				}
			}
		List<String> animatedProperties = new ArrayList<String>();
		List<Double> averages = new ArrayList<Double>();
		for (Entry<String, List<Double>> entry : animationSmoothnessValuesMap.entrySet()) {
			List<Double> animationSmoothnessValues = entry.getValue();
			Integer sum = sum(animationSmoothnessValues);
			Double averageAnimationSmoothness = Double.valueOf(sum/animationSmoothnessValues.size());
			animatedProperties.add(entry.getKey());
			averages.add(averageAnimationSmoothness);
		}
		return new ImmutablePair<List<String>, List<Double>>(animatedProperties, averages);
	}
	
	public Pair<List<String>, List<Double>> createStaticAnimationStatistics() {
		Map<String/*Animation*/, List<Double>/*Values*/> animationSmoothnessValuesMap = new HashMap<String, List<Double>>();
		for (String date : allStaticExperimentDates) {
			List<StaticExperiment> animatedExperiments = MongoDBConnector.getInstance().queryStaticExperimentsByDate(date);
			for(StaticExperiment animatedExperiment : animatedExperiments) {
				for (StaticExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getRenderTimeInFrames();
						String animationKey = getPropertyKey(animatedExperiment.getStyleProperties());
						List<Double> animationSmoothnessValues = animationSmoothnessValuesMap.get(animationKey);
						if (animationSmoothnessValues == null) {
							animationSmoothnessValues = new ArrayList<Double>();
							animationSmoothnessValuesMap.put(animationKey, animationSmoothnessValues);
						}
						animationSmoothnessValues.add(Double.valueOf(animationSmoothness));
					}
				}
			}
		List<String> animatedProperties = new ArrayList<String>();
		List<Double> averages = new ArrayList<Double>();
		for (Entry<String, List<Double>> entry : animationSmoothnessValuesMap.entrySet()) {
			List<Double> animationSmoothnessValues = entry.getValue();
			Integer sum = sum(animationSmoothnessValues);
			Double averageAnimationSmoothness = Double.valueOf(sum/animationSmoothnessValues.size());
			animatedProperties.add(entry.getKey());
			averages.add(averageAnimationSmoothness);
		}
		return new ImmutablePair<List<String>, List<Double>>(animatedProperties, averages);
	}
	
	public List<XYSeriesData> createAnimatedNumberOfElementsStatistics() {
		List<XYSeriesData> xySeriesDataList = new ArrayList<XYSeriesData>();
		Map<String/*Animation*/, Map<Double/*Number*/, Double/*Values*/>> preResult = new TreeMap<String, Map<Double/*Number*/, Double/*Values*/>>();
		Map<Pair<String/*Animation*/, Double/*Number*/>, List<Double>/*Values*/> sortedAnimationSmoothnessValues = new HashMap<Pair<String, Double>, List<Double>>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					double numberOfObjects = animatedExperimentResult.getNumberOfObjects();
					if(numberOfObjects != 1) {
						numberOfObjects--;
					}
					if(numberOfObjects <= 2000 && numberOfObjects != 500 && numberOfObjects != 1500) {
						String animationKey = getAnimationKey(animatedExperiment.getAnimation());
						Pair<String, Double> key = new ImmutablePair<String, Double>(animationKey, Double.valueOf(numberOfObjects));
						List<Double> animationSmoothnessValues = sortedAnimationSmoothnessValues.get(key);
						if (animationSmoothnessValues == null) {
							animationSmoothnessValues = new ArrayList<Double>();
							sortedAnimationSmoothnessValues.put(key, animationSmoothnessValues);
						}
						animationSmoothnessValues.add(Double.valueOf(animationSmoothness));
					}
					}
				}
			}
		for (Entry<Pair<String, Double>, List<Double>> entry : sortedAnimationSmoothnessValues.entrySet()) {
			List<Double> animationSmoothnessValues = entry.getValue();
			Integer sum = sum(animationSmoothnessValues);
			Double averageAnimationSmoothness = Double.valueOf(sum/animationSmoothnessValues.size());
			Map<Double, Double> numbersToAverages = preResult.get(entry.getKey().getLeft());
			if(numbersToAverages == null) {
				numbersToAverages = new TreeMap<Double, Double>();
				preResult.put(entry.getKey().getLeft(), numbersToAverages);
			} else 
			numbersToAverages.put(entry.getKey().getRight(), averageAnimationSmoothness);
		}
		for (Entry<String, Map<Double, Double>> entry : preResult.entrySet()) {
			String name = entry.getKey();
			List<Double> xCoordinates = new ArrayList<Double>();
			List<Double> yCoordinates = new ArrayList<Double>();
			for (Entry<Double, Double> valuesEntry : entry.getValue().entrySet()) {
				xCoordinates.add(valuesEntry.getKey());
				yCoordinates.add(valuesEntry.getValue());
			}
			XYSeriesData xySeriesData = new XYSeriesData(name, convertDoubleListToArray(xCoordinates), convertDoubleListToArray(yCoordinates), null, null);
			xySeriesDataList.add(xySeriesData);
		}
		return xySeriesDataList;
	}
	
	public List<XYSeriesData> createBrowserVarianceStatistic() {
		List<XYSeriesData> result = new ArrayList<XYSeriesData>();
		int counter = 0;
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperimentsList = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
				for(AnimatedExperiment animatedExperiment : animatedExperimentsList) {
					String name = animatedExperiment.getWebTechnologyCombination().name()+" + "+animatedExperiment.getBrowser().name()+counter;
					Color color = getColorForBrowser(animatedExperiment.getBrowser());
					Marker marker = null;
					int coordinesSize = animatedExperiment.getExperimentResults().size();
					double[] xCoordinates = new double[coordinesSize];
					double[] yCoordinates = new double[coordinesSize];
					for(int i = 0; i < coordinesSize; i++) {
						int numberOfObjects = animatedExperiment.getExperimentResults().get(i).getNumberOfObjects();
						if (numberOfObjects <= 2001) {
						xCoordinates[i] = animatedExperiment.getExperimentResults().get(i).getNumberOfObjects();
						yCoordinates[i] = animatedExperiment.getExperimentResults().get(i).getIdenticalFrameRatePer60Frames();
							}
					}
					XYSeriesData xySeriesData = new XYSeriesData(name, xCoordinates, yCoordinates, color, marker);
					result.add(xySeriesData);
					counter++;
				}
		}
		return result;	
	}
	
	public List<XYSeriesData> createStaticNumberOfElementsStatistics() {
		List<XYSeriesData> xySeriesDataList = new ArrayList<XYSeriesData>();
		Map<String/*Animation*/, Map<Double/*Number*/, Double/*Values*/>> preResult = new TreeMap<String, Map<Double/*Number*/, Double/*Values*/>>();
		Map<Pair<String/*Animation*/, Double/*Number*/>, List<Double>/*Values*/> sortedAnimationSmoothnessValues = new HashMap<Pair<String, Double>, List<Double>>();
		for (String date : allStaticExperimentDates) {
			List<StaticExperiment> animatedExperiments = MongoDBConnector.getInstance().queryStaticExperimentsByDate(date);
			for(StaticExperiment animatedExperiment : animatedExperiments) {
				for (StaticExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getRenderTimeInFrames();
					double numberOfObjects = animatedExperimentResult.getNumberOfObjects();
						String animationKey = getPropertyKey(animatedExperiment.getStyleProperties());
						Pair<String, Double> key = new ImmutablePair<String, Double>(animationKey, Double.valueOf(numberOfObjects));
						List<Double> animationSmoothnessValues = sortedAnimationSmoothnessValues.get(key);
						if (animationSmoothnessValues == null) {
							animationSmoothnessValues = new ArrayList<Double>();
							sortedAnimationSmoothnessValues.put(key, animationSmoothnessValues);
						}
						animationSmoothnessValues.add(Double.valueOf(animationSmoothness));
					}
				}
			}
		for (Entry<Pair<String, Double>, List<Double>> entry : sortedAnimationSmoothnessValues.entrySet()) {
			List<Double> animationSmoothnessValues = entry.getValue();
			Integer sum = sum(animationSmoothnessValues);
			Double averageAnimationSmoothness = Double.valueOf(sum/animationSmoothnessValues.size());
			Map<Double, Double> numbersToAverages = preResult.get(entry.getKey().getLeft());
			if(numbersToAverages == null) {
				numbersToAverages = new TreeMap<Double, Double>();
				preResult.put(entry.getKey().getLeft(), numbersToAverages);
			} else 
			numbersToAverages.put(entry.getKey().getRight(), averageAnimationSmoothness);
		}
		for (Entry<String, Map<Double, Double>> entry : preResult.entrySet()) {
			String name = entry.getKey();
			List<Double> xCoordinates = new ArrayList<Double>();
			List<Double> yCoordinates = new ArrayList<Double>();
			for (Entry<Double, Double> valuesEntry : entry.getValue().entrySet()) {
				xCoordinates.add(valuesEntry.getKey());
				yCoordinates.add(valuesEntry.getValue());
			}
			XYSeriesData xySeriesData = new XYSeriesData(name, convertDoubleListToArray(xCoordinates), convertDoubleListToArray(yCoordinates), null, null);
			xySeriesDataList.add(xySeriesData);
		}
		return xySeriesDataList;
	}

	public Map<String, Pair<List<String>, List<Double>>> createAnimatedWebTechnologyStatistics() {
		Map<String, Pair<List<String>, List<Double>>> result = new HashMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, Double>> preResult = new HashMap<String, Map<String, Double>>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					String webTechnologyCombination = animatedExperiment.getWebTechnologyCombination().name();
					Map<String, Double> graphicPrimitivesValues = preResult.get(webTechnologyCombination);
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, Double>();
						preResult.put(webTechnologyCombination, graphicPrimitivesValues);
					}
					String animationKey = getAnimationKey(animatedExperiment.getAnimation());
					Double renderTimeInFramesSum = graphicPrimitivesValues.get(animationKey);
					if (renderTimeInFramesSum == null) {
						graphicPrimitivesValues.put(animationKey, Double.valueOf(animationSmoothness));
					} else {
						double newRenderTimeInFramesSum = renderTimeInFramesSum.doubleValue() + animationSmoothness;
						graphicPrimitivesValues.remove(animationKey);
						graphicPrimitivesValues.put(animationKey, Double.valueOf(newRenderTimeInFramesSum));
					}
					
				}
			}
		}
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResult.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	public Pair<List<String>, List<Double>> createAnimatedPropertiesStatistics() {
		Map<String, Double> preResult = new HashMap<String, Double>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					String animationKey = getAnimationKey(animatedExperiment.getAnimation());
					Double animationSmoothnessSum = preResult.get(animationKey);
					if(animationSmoothnessSum == null) {
						preResult.put(animationKey, Double.valueOf(animationSmoothness));
					} else {
						preResult.remove(animationKey);
						preResult.put(animationKey, Double.valueOf(animationSmoothnessSum.doubleValue()+animationSmoothness));
					}
				}
			}
		}
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		List<Double> animationSmoothnessSums = new ArrayList<Double>();
		animationSmoothnessSums.addAll(preResult.values());
		return new ImmutablePair<List<String>, List<Double>>(animations, animationSmoothnessSums);
	}
	
	public Map<String, Pair<List<String>, List<Double>>> createAnimatedGraphicPrimitiveStatistics() {
		Map<String, Pair<List<String>, List<Double>>> result = new HashMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, Double>> preResult = new HashMap<String, Map<String, Double>>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					String graphicPrimitive = animatedExperiment.getGraphicPrimitive().name();
					Map<String, Double> graphicPrimitivesValues = preResult.get(graphicPrimitive);
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, Double>();
						preResult.put(graphicPrimitive, graphicPrimitivesValues);
					}
					String animationKey = getAnimationKey(animatedExperiment.getAnimation());
					Double renderTimeInFramesSum = graphicPrimitivesValues.get(animationKey);
					if (renderTimeInFramesSum == null) {
						graphicPrimitivesValues.put(animationKey, Double.valueOf(animationSmoothness));
					} else {
						double newRenderTimeInFramesSum = renderTimeInFramesSum.doubleValue() + animationSmoothness;
						graphicPrimitivesValues.remove(animationKey);
						graphicPrimitivesValues.put(animationKey, Double.valueOf(newRenderTimeInFramesSum));
					}
					
				}
			}
		}
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResult.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	public Map<String, Pair<List<String>, List<Double>>> createStaticAvgStatisticsWebTechnologyCombination() {
		Map<String, Pair<List<String>, List<Double>>> result = new HashMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, List<Double>>> preResult = new HashMap<String, Map<String, List<Double>>>();
		for (String date : allStaticExperimentDates) {
			List<StaticExperiment> animatedExperiments = MongoDBConnector.getInstance().queryStaticExperimentsByDate(date);
			for(StaticExperiment animatedExperiment : animatedExperiments) {
				for (StaticExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getRenderTimeInFrames();
					Map<String, List<Double>> graphicPrimitivesValues = preResult.get(animatedExperiment.getBrowser().name());
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, List<Double>>();
						preResult.put(animatedExperiment.getBrowser().name(), graphicPrimitivesValues);
					}
					String animationKey = getPropertyKey(animatedExperiment.getStyleProperties());
					List<Double> graphicPrimitiveAnimationSmoothnessValuesList = graphicPrimitivesValues.get(animationKey);
					if (graphicPrimitiveAnimationSmoothnessValuesList == null) {
						graphicPrimitiveAnimationSmoothnessValuesList = new ArrayList<Double>();
					}
						graphicPrimitivesValues.remove(animationKey);
						graphicPrimitiveAnimationSmoothnessValuesList.add(Double.valueOf(animationSmoothness));
						graphicPrimitivesValues.put(animationKey, graphicPrimitiveAnimationSmoothnessValuesList);
				}
			}
		}
		Map<String, Map<String, Double>> preResultAverages = convertMapWithValuesToAverages(preResult);
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResultAverages.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}

	public Map<String, Pair<List<String>, List<Double>>> createAvgStatisticsWebTechnologyCombination() {
		Map<String, Pair<List<String>, List<Double>>> result = new HashMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, List<Double>>> preResult = new HashMap<String, Map<String, List<Double>>>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					Map<String, List<Double>> graphicPrimitivesValues = preResult.get(animatedExperiment.getWebTechnologyCombination().name());
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, List<Double>>();
						preResult.put(animatedExperiment.getWebTechnologyCombination().name(), graphicPrimitivesValues);
					}
					String animationKey = getAnimationKey(animatedExperiment.getAnimation());
					List<Double> graphicPrimitiveAnimationSmoothnessValuesList = graphicPrimitivesValues.get(animationKey);
					if (graphicPrimitiveAnimationSmoothnessValuesList == null) {
						graphicPrimitiveAnimationSmoothnessValuesList = new ArrayList<Double>();
					}
						graphicPrimitivesValues.remove(animationKey);
						graphicPrimitiveAnimationSmoothnessValuesList.add(Double.valueOf(animationSmoothness));
						graphicPrimitivesValues.put(animationKey, graphicPrimitiveAnimationSmoothnessValuesList);
				}
			}
		}
		Map<String, Map<String, Double>> preResultAverages = convertMapWithValuesToAverages(preResult);
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResultAverages.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	public Map<String, Pair<List<String>, List<Double>>> createAvgStatisticsBrowser() {
		Map<String, Pair<List<String>, List<Double>>> result = new HashMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, List<Double>>> preResult = new HashMap<String, Map<String, List<Double>>>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					Map<String, List<Double>> graphicPrimitivesValues = preResult.get(animatedExperiment.getBrowser().name());
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, List<Double>>();
						preResult.put(animatedExperiment.getBrowser().name(), graphicPrimitivesValues);
					}
					String animationKey = getAnimationKey(animatedExperiment.getAnimation());
					List<Double> graphicPrimitiveAnimationSmoothnessValuesList = graphicPrimitivesValues.get(animationKey);
					if (graphicPrimitiveAnimationSmoothnessValuesList == null) {
						graphicPrimitiveAnimationSmoothnessValuesList = new ArrayList<Double>();
					}
						graphicPrimitivesValues.remove(animationKey);
						graphicPrimitiveAnimationSmoothnessValuesList.add(Double.valueOf(animationSmoothness));
						graphicPrimitivesValues.put(animationKey, graphicPrimitiveAnimationSmoothnessValuesList);
				}
			}
		}
		Map<String, Map<String, Double>> preResultAverages = convertMapWithValuesToAverages(preResult);
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResultAverages.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	public Map<String, Pair<List<String>, List<Double>>> createAvgStatisticsGraphicPrimitives() {
		Map<String, Pair<List<String>, List<Double>>> result = new HashMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, List<Double>>> preResult = new HashMap<String, Map<String, List<Double>>>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					Map<String, List<Double>> graphicPrimitivesValues = preResult.get(animatedExperiment.getGraphicPrimitive().name());
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, List<Double>>();
						preResult.put(animatedExperiment.getGraphicPrimitive().name(), graphicPrimitivesValues);
					}
					String animationKey = getAnimationKey(animatedExperiment.getAnimation());
					List<Double> graphicPrimitiveAnimationSmoothnessValuesList = graphicPrimitivesValues.get(animationKey);
					if (graphicPrimitiveAnimationSmoothnessValuesList == null) {
						graphicPrimitiveAnimationSmoothnessValuesList = new ArrayList<Double>();
					}
						graphicPrimitivesValues.remove(animationKey);
						graphicPrimitiveAnimationSmoothnessValuesList.add(Double.valueOf(animationSmoothness));
						graphicPrimitivesValues.put(animationKey, graphicPrimitiveAnimationSmoothnessValuesList);
				}
			}
		}
		Map<String, Map<String, Double>> preResultAverages = convertMapWithValuesToAverages(preResult);
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResultAverages.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	public Map<String, Pair<List<String>, List<Double>>> createAvgStatisticsAnimationProperties() {
		Map<String, Pair<List<String>, List<Double>>> result = new HashMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, List<Double>>> preResult = new HashMap<String, Map<String, List<Double>>>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					Map<String, List<Double>> graphicPrimitivesValues = preResult.get(getAnimationKey(animatedExperiment.getAnimation()));
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, List<Double>>();
						preResult.put(getAnimationKey(animatedExperiment.getAnimation()), graphicPrimitivesValues);
					}
					String animationKey = getAnimationKey(animatedExperiment.getAnimation());
					List<Double> graphicPrimitiveAnimationSmoothnessValuesList = graphicPrimitivesValues.get(animationKey);
					if (graphicPrimitiveAnimationSmoothnessValuesList == null) {
						graphicPrimitiveAnimationSmoothnessValuesList = new ArrayList<Double>();
					}
						graphicPrimitivesValues.remove(animationKey);
						graphicPrimitiveAnimationSmoothnessValuesList.add(Double.valueOf(animationSmoothness));
						graphicPrimitivesValues.put(animationKey, graphicPrimitiveAnimationSmoothnessValuesList);
				}
			}
		}
		Map<String, Map<String, Double>> preResultAverages = convertMapWithValuesToAverages(preResult);
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResultAverages.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	public Pair<List<String>, List<Double>> createBrowserStatisticExperimentDates() {
		List<String> browserStatisticDates = new ArrayList<String>();
		browserStatisticDates.addAll(positionAnimationExperimentDates);
		browserStatisticDates.addAll(opacityAnimationExperimentDates);
		browserStatisticDates.addAll(scaleAnimationExperimentDates);
		browserStatisticDates.addAll(heightAnimationExperimentDates);
		browserStatisticDates.addAll(positionOpacityAnimationExperimentDates);
		browserStatisticDates.addAll(positionOpacityScaleAnimationExperimentDates);
		
		Pair<List<String>, List<Double>> result = null;
		Map<String, Double> statisticsMap = new TreeMap<String, Double>();
		
		for (String date : browserStatisticDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					addValueToSumMapForKey(statisticsMap, animatedExperiment.getBrowser().name(), animatedExperimentResult.getIdenticalFrameRatePer60Frames());
				}
			}
		}
		result = convertMapToListPairs(statisticsMap);
		return result;
	}
	
	public Map<String, Pair<List<String>, List<Double>>> createAvgStatisticsNumberOfElements() {
		Map<String, Pair<List<String>, List<Double>>> result = new HashMap<String, Pair<List<String>, List<Double>>>();
		Map<String, Map<String, List<Double>>> preResult = new HashMap<String, Map<String, List<Double>>>();
		for (String date : allAnimatedExperimentDates) {
			List<AnimatedExperiment> animatedExperiments = MongoDBConnector.getInstance().queryAnimatedExperimentsByDate(date);
			for(AnimatedExperiment animatedExperiment : animatedExperiments) {
				for (AnimatedExperimentResult animatedExperimentResult : animatedExperiment.getExperimentResults()) {
					double animationSmoothness = animatedExperimentResult.getIdenticalFrameRatePer60Frames();
					Map<String, List<Double>> graphicPrimitivesValues = preResult.get(animatedExperimentResult.getNumberOfObjects()+"");
					if(graphicPrimitivesValues == null) {
						graphicPrimitivesValues = new HashMap<String, List<Double>>();
						preResult.put(animatedExperimentResult.getNumberOfObjects()+"", graphicPrimitivesValues);
					}
					String animationKey = getAnimationKey(animatedExperiment.getAnimation());
					List<Double> graphicPrimitiveAnimationSmoothnessValuesList = graphicPrimitivesValues.get(animationKey);
					if (graphicPrimitiveAnimationSmoothnessValuesList == null) {
						graphicPrimitiveAnimationSmoothnessValuesList = new ArrayList<Double>();
					}
						graphicPrimitivesValues.remove(animationKey);
						graphicPrimitiveAnimationSmoothnessValuesList.add(Double.valueOf(animationSmoothness));
						graphicPrimitivesValues.put(animationKey, graphicPrimitiveAnimationSmoothnessValuesList);
				}
			}
		}
		Map<String, Map<String, Double>> preResultAverages = convertMapWithValuesToAverages(preResult);
		List<String> animations = new ArrayList<String>();
		animations.addAll(preResult.keySet());
		for (Entry<String, Map<String, Double>> entry : preResultAverages.entrySet()) {
			result.put(entry.getKey(), convertMapToListPairs(entry.getValue()));
		}
		return result;
	}
	
	private Map<String, Map<String, Double>> convertMapWithValuesToAverages(Map<String, Map<String, List<Double>>> map) {
		Map<String, Map<String, Double>> result = new HashMap<String, Map<String, Double>>();
		for (Entry<String, Map<String, List<Double>>> entry : map.entrySet()) {
			Map<String, List<Double>> graphicPrimitivesValuesMap = map.get(entry.getKey());
			Map<String, Double> graphicPrimtitivesToAverages = new HashMap<String, Double>();
			for (Entry<String, List<Double>> graphicPrimitivesEntry : graphicPrimitivesValuesMap.entrySet()) {
				Integer valuesSum = sum(graphicPrimitivesEntry.getValue());
				Double average = Double.valueOf(valuesSum.intValue()/graphicPrimitivesEntry.getValue().size());
				graphicPrimtitivesToAverages.put(graphicPrimitivesEntry.getKey(), average);
			}
			result.put(entry.getKey(), graphicPrimtitivesToAverages);
		}
		return result;
	}
	
	private double[] convertDoubleListToArray(List<Double> list) {
		double[] array = new double[list.size()];
		for(int i = 0; i < list.size(); i++) {
			array[i] = list.get(i).doubleValue();
		}
		return array;
	}
	
	private void addValueToSumMapForKey(Map<String, Double> map, String key, double value) {
		Double currentSum = map.get(key);
		if (currentSum == null) {
			map.put(key, Double.valueOf(value));
		} else {
			double newSum = currentSum.doubleValue();
			newSum = newSum + value;
			map.remove(key);
			map.put(key, newSum);
		}
	}
	
	private Color getColorForBrowser(Browser browser) {
		Color result = null;
		switch (browser) {
		case CHROME:
			result = Color.BLUE;
			break;
		case FIREFOX:
			result = Color.ORANGE;
			break;
		case OPERA:
			result = Color.RED;
			break;
		default:
			break;
		}
		return result;
	}
	
	private String getPropertyKey(List<Property> styledProperty) {
		StringBuilder stringBuilder = new StringBuilder();
		if(styledProperty.isEmpty()) {
			stringBuilder.append("NO STYLE ");
		} else {
			for(Property property : styledProperty) {
				stringBuilder.append(property.name()+" ");
			}
		}
		return stringBuilder.toString();
	}
	
	private String getAnimationKey(Animation animation) {
		StringBuilder stringBuilder = new StringBuilder();
		for (AnimatedProperty property : animation.getProperties()) {
			stringBuilder.append(property.name().charAt(0) + " ");
		}
		return stringBuilder.toString();
	}
	
	private Pair<List<String>, List<Double>> convertMapToListPairs(Map<String, Double> map) {
		Pair<List<String>, List<Double>> result = null;
		List<String> xAxisValues = new ArrayList<String>();
		List<Double> yAxisValues = new ArrayList<Double>();
		for (Entry<String, Double> entry : map.entrySet()) {
			xAxisValues.add(entry.getKey());
			yAxisValues.add(entry.getValue());
		}
		result = new ImmutablePair<List<String>, List<Double>>(xAxisValues, yAxisValues);
		return result;
	}
	
	private Integer sum(List<Double> list) {
		int sum= 0; 
	     for (Double i:list)
	         sum = sum + i.intValue();
	     return Integer.valueOf(sum);
	}
	
	private void initExperimentDates() {
		staticGraphicPrimitivesDates = new ArrayList<String>();
		staticGraphicPrimitivesDates.add("Jul 9, 2016 7:10:34 AM");
		staticGraphicPrimitivesDates.add("Jul 9, 2016 8:54:01 AM");
		staticGraphicPrimitivesDates.add("Jul 9, 2016 10:37:24 AM");
		
		staticOpacityGraphicPrimitivesDates = new ArrayList<String>();
		staticOpacityGraphicPrimitivesDates.add("Jul 9, 2016 5:33:32 PM");
		staticOpacityGraphicPrimitivesDates.add("Jul 9, 2016 7:17:26 PM");
		staticOpacityGraphicPrimitivesDates.add("Jul 9, 2016 9:01:30 PM");
		
		staticShadowGraphicPrimitivesDates = new ArrayList<String>();
		staticShadowGraphicPrimitivesDates.add("Jul 9, 2016 12:21:12 PM");
		staticShadowGraphicPrimitivesDates.add("Jul 9, 2016 2:05:00 PM");
		staticShadowGraphicPrimitivesDates.add("Jul 9, 2016 3:49:19 PM");
		
		positionAnimationExperimentDates = new ArrayList<String>();
		positionAnimationExperimentDates.add("Jul 2, 2016 1:57:33 PM");
		positionAnimationExperimentDates.add("Jul 1, 2016 8:21:28 PM");
		positionAnimationExperimentDates.add("Jul 2, 2016 6:56:09 PM");
		positionAnimationExperimentDates.add("Jul 11, 2016 2:28:21 PM");
		
		opacityAnimationExperimentDates = new ArrayList<String>();
		opacityAnimationExperimentDates.add("Jul 3, 2016 1:24:42 PM");
		opacityAnimationExperimentDates.add("Jun 30, 2016 8:25:28 PM");
		opacityAnimationExperimentDates.add("Jul 3, 2016 10:08:55 AM");
		opacityAnimationExperimentDates.add("Jul 11, 2016 10:58:18 AM");
		
		scaleAnimationExperimentDates = new ArrayList<String>();
		scaleAnimationExperimentDates.add("Jul 4, 2016 4:23:47 PM");
		scaleAnimationExperimentDates.add("Jul 4, 2016 6:52:39 PM");
		
		heightAnimationExperimentDates = new ArrayList<String>();
		heightAnimationExperimentDates.add("Jul 5, 2016 9:43:15 AM");
		
		lineLenghtAnimationExperimentDate = "Jul 10, 2016 4:39:48 PM";
		
		positionOpacityAnimationExperimentDates = new ArrayList<String>();
		positionOpacityAnimationExperimentDates.add("Jul 5, 2016 12:49:10 PM");
		positionOpacityAnimationExperimentDates.add("Jul 5, 2016 3:14:29 PM");
		
		positionOpacityScaleAnimationExperimentDates = new ArrayList<String>();
		positionOpacityScaleAnimationExperimentDates.add("Jul 5, 2016 8:26:36 PM");
		positionOpacityScaleAnimationExperimentDates.add("Jul 5, 2016 10:52:04 PM");
		
		allStaticExperimentDates = new ArrayList<String>();
		allStaticExperimentDates.addAll(staticGraphicPrimitivesDates);
		allStaticExperimentDates.addAll(staticOpacityGraphicPrimitivesDates);
		allStaticExperimentDates.addAll(staticShadowGraphicPrimitivesDates);
		
		allAnimatedExperimentDates = new ArrayList<String>();
		allAnimatedExperimentDates.addAll(positionAnimationExperimentDates);
		allAnimatedExperimentDates.addAll(opacityAnimationExperimentDates);
		allAnimatedExperimentDates.addAll(scaleAnimationExperimentDates);
		allAnimatedExperimentDates.addAll(heightAnimationExperimentDates);
		allAnimatedExperimentDates.add(lineLenghtAnimationExperimentDate);
		allAnimatedExperimentDates.addAll(positionOpacityAnimationExperimentDates);
		allAnimatedExperimentDates.addAll(positionOpacityScaleAnimationExperimentDates);
		
		List<String> squareCircleLineStatisticDates = new ArrayList<String>();
		squareCircleLineStatisticDates.addAll(positionAnimationExperimentDates);
		squareCircleLineStatisticDates.addAll(opacityAnimationExperimentDates);
	}
}
