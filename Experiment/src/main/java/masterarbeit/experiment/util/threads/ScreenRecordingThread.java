package masterarbeit.experiment.util.threads;

import masterarbeit.experiment.util.CommandExecutor;


public class ScreenRecordingThread extends Thread {
	private int durationInMilliseconds;
	private String filePath;
	
	private static final String RECORDING_TIME_PLACEHOLDER = "RECORDING_TIME";
	private static final String OUTPUT_FILE_PATH_PLACEHOLDER = "OUTPUT_FILE_PATH";
	private static final String RECORD_COMMAND_TEMPLATE = CommandExecutor.FFMPEG_EXECUTABLE_PATH+" -t "+RECORDING_TIME_PLACEHOLDER+" -framerate 60 -video_device_index 1 -f avfoundation -i \"\" -c:v libx264 -preset ultrafast -crf 0 "+OUTPUT_FILE_PATH_PLACEHOLDER;
	
	public ScreenRecordingThread(int durationInMilliseconds, String filePath) {
		this.durationInMilliseconds = durationInMilliseconds;
		this.filePath = filePath;
	}
	
	public void run() {
		startScreenRecordingProcessForDuration(durationInMilliseconds, filePath);
	}
	
	public static void startScreenRecordingProcessForDuration(int durationInMilliseconds, String filePath) {
		String recordCommand = createRecordCommandWithRecordingTimeInSecondsAndOutputFile(durationInMilliseconds, filePath);
		Process process = CommandExecutor.runCommand(recordCommand);
		try {
			process.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Screen recording process finished");
	}
	
	private static String createRecordCommandWithRecordingTimeInSecondsAndOutputFile(int recordingTimeInSeconds, String outputFilePath) {
		String result = null;
		result = RECORD_COMMAND_TEMPLATE.replaceAll(RECORDING_TIME_PLACEHOLDER, recordingTimeInSeconds + "");
		result = result.replaceAll(OUTPUT_FILE_PATH_PLACEHOLDER, outputFilePath);
		return result;
	}
}
