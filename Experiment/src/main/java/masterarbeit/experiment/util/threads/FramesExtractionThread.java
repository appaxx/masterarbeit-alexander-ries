package masterarbeit.experiment.util.threads;

import java.io.File;

import masterarbeit.experiment.util.CommandExecutor;
import masterarbeit.experiment.util.FileUtil;


public class FramesExtractionThread extends Thread {
	private String pathToVideo;
	private String framesDirectoryPath; 
	
	private static final String VIDEO_FILE_PATH_PLACEHOLDER = "VIDEO_FILE_PATH";
	private static final String FRAMES_DIRECTORY_PLACEHOLDER = "FRAMES_DIRECTORY";
	private static final String EXTRACT_VIDEO_FRAMES_COMMAND_TEMPLATE = CommandExecutor.FFMPEG_EXECUTABLE_PATH+" -i "+VIDEO_FILE_PATH_PLACEHOLDER+" -r 63 "+FRAMES_DIRECTORY_PLACEHOLDER+"/frame_%d.bmp";

	public FramesExtractionThread(String pathToVideo, String framesDirectoryPath) {
		this.pathToVideo = pathToVideo;
		this.framesDirectoryPath = framesDirectoryPath;
	}
	
	public void run() {
		extractFramesFromVideo();
	}
	
	public File[] getFrames() {
		File framesDirectory = new File(framesDirectoryPath);
		File[] frames = framesDirectory.listFiles();
		FileUtil.sortImageFiles(frames);
		return frames;
	}
	
	public void extractFramesFromVideo() {
		FileUtil.createDirectoryWithPath(framesDirectoryPath);
		String extractFramesCommand = createExtractFramesCommandWithVideoFilePathAndFramesDirectory(pathToVideo, framesDirectoryPath);
		Process process = CommandExecutor.runCommand(extractFramesCommand);
		try {
			process.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Frames extraction process finished");
	}
	
	private static String createExtractFramesCommandWithVideoFilePathAndFramesDirectory(String videoFilePath, String framesDirectory) {
		String result = null;
		result = EXTRACT_VIDEO_FRAMES_COMMAND_TEMPLATE.replaceAll(VIDEO_FILE_PATH_PLACEHOLDER, videoFilePath);
		result = result.replaceAll(FRAMES_DIRECTORY_PLACEHOLDER, framesDirectory);
		return result;
	}
}
