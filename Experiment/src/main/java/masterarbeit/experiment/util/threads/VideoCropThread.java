package masterarbeit.experiment.util.threads;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import masterarbeit.experiment.util.CommandExecutor;
import masterarbeit.experiment.util.FileUtil;

public class VideoCropThread extends Thread {
	private File[] frames;
	private String framesFolderPath;
	
	private static final String INPUT_IMAGE_PATH = "INPUT_IMAGE_PATH";
	private static final String OUTPUT_IMAGE_PATH = "OUTPUT_IMAGE_PATH";
	private static final int VIDEO_WIDTH = 2880;
	private static final int VIDEO_HEIGHT = 1800;
	private static final int BROWSER_BAR_HEIGHT = 240;
	private static final String CROP_IMAGE_COMMAND_TEMPLATE = CommandExecutor.FFMPEG_EXECUTABLE_PATH+" -i INPUT_IMAGE_PATH -vf crop="+VIDEO_WIDTH+":"+(VIDEO_HEIGHT-BROWSER_BAR_HEIGHT)+":0:"+BROWSER_BAR_HEIGHT+" OUTPUT_IMAGE_PATH";
	private static final Logger logger = Logger.getLogger(VideoCropThread.class.getName()); 
	
	public VideoCropThread(File[] frames, String framesFolderPath) {
		this.frames = frames;
		this.framesFolderPath = framesFolderPath;
	}
	
	public void run() {
		FileUtil.createDirectoryWithPath(framesFolderPath);
		for (int i = 0; i < frames.length; i++) {
			final File frame = frames[i];
			String cropVideoCommand = createCropCommandWithInputAndOutputFilePath(frame.getAbsolutePath(), framesFolderPath+"/frame_"+i+".bmp");
			logger.log(Level.INFO, cropVideoCommand);
			Process process = CommandExecutor.runCommand(cropVideoCommand);
			try {
				process.waitFor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Frame cropping has finished");
	}
	
	public File[] getCroppedFrames() {
		File framesDirectory = new File(framesFolderPath);
		File[] frames = framesDirectory.listFiles();
		FileUtil.sortImageFiles(frames);    
		return frames;
	}
	
	private static String createCropCommandWithInputAndOutputFilePath(String inputFilePath, String outputFilePath) {
		String result = CROP_IMAGE_COMMAND_TEMPLATE;
		result = result.replaceAll(INPUT_IMAGE_PATH, inputFilePath);
		result = result.replaceAll(OUTPUT_IMAGE_PATH, outputFilePath);
		return result;
	}
}
