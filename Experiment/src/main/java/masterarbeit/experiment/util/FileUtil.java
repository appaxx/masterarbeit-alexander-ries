package masterarbeit.experiment.util;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import org.apache.commons.io.FileUtils;

public class FileUtil {

	public static void sortImageFiles(File[] imageFiles) {
		Arrays.sort(imageFiles, new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				int n1 = extractNumber(o1.getName());
				int n2 = extractNumber(o2.getName());
				return n1 - n2;
			}

			private int extractNumber(String name) {
				int i = 0;
				try {
					int s = name.indexOf('_') + 1;
					int e = name.lastIndexOf('.');
					String number = name.substring(s, e);
					i = Integer.parseInt(number);
				} catch (Exception e) {
					i = 0;
				}
				return i;
			}
		});
	}

	public static void createDirectoryWithPath(String directoryPath) {
		File directory = new File(directoryPath);
		directory.mkdir();
	}

	public static void deleteDirectoriesThatMatchRegexInFolder(String folderPath, String regex) {
		File folder = new File(folderPath);
		String[] listOfElements = folder.list();
		for (int i = 0; i < listOfElements.length; i++) {
			String elementName = listOfElements[i];
			if (elementName.contains(regex)) {
				File directoryToDelete = new File(folderPath+""+listOfElements[i]);
				try {
					FileUtils.deleteDirectory(directoryToDelete);
				} catch (IOException e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void deleteFilesThatMatchRegexInFolder(String folderPath, String regex) {
		File folder = new File(folderPath);
		String[] listOfElements = folder.list();
		for (int i = 0; i < listOfElements.length; i++) {
				String elementName = listOfElements[i];
				if (elementName.contains(regex)) {
					File fileToDelete = new File(folderPath+""+listOfElements[i]);
					fileToDelete.delete();
				}
		}
	}
	
}
