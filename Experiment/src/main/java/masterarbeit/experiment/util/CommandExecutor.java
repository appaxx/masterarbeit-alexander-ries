package masterarbeit.experiment.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import masterarbeit.experiment.util.threads.ReaderThread;

public class CommandExecutor {
	
	public static final String FFMPEG_EXECUTABLE_PATH = "/usr/local/bin/ffmpeg";

	public static Process runCommand(String command) {
		Process result = null;
		try {
			result = Runtime.getRuntime().exec(command.split(" "));
			BufferedReader processInputStream = new BufferedReader(new InputStreamReader(result.getInputStream()));
			ReaderThread inputReader = new ReaderThread(processInputStream);
			inputReader.start();
			BufferedReader processErrorStream = new BufferedReader(new InputStreamReader(result.getErrorStream()));
			ReaderThread errorReader = new ReaderThread(processErrorStream);
			errorReader.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
