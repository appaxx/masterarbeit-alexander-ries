package masterarbeit.experiment;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import masterarbeit.db.MongoDBConnector;
import masterarbeit.dto.Browser;
import masterarbeit.dto.experiment.AnimatedExperiment;
import masterarbeit.dto.experiment.AnimatedExperimentType;
import masterarbeit.dto.experiment.AnimatedProperty;
import masterarbeit.dto.experiment.Property;
import masterarbeit.dto.experiment.StaticExperiment;
import masterarbeit.dto.experiment.StaticExperimentType;
import masterarbeit.dto.experiment.result.AnimatedExperimentResult;
import masterarbeit.dto.experiment.result.StaticExperimentResult;
import masterarbeit.experiment.util.FileUtil;
import masterarbeit.experiment.util.threads.FramesExtractionThread;
import masterarbeit.experiment.util.threads.ScreenRecordingThread;
import masterarbeit.experiment.util.threads.VideoCropThread;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExperimentExecutor {
	private Date experimentDate;

	public void run() {
		List<StaticExperiment> staticExperiments = new ArrayList<StaticExperiment>();
		staticExperiments.add(StaticExperiment.getInstanceOfStaticExperimentType(StaticExperimentType.SVG_SQUARE));
		staticExperiments.add(StaticExperiment.getInstanceOfStaticExperimentType(StaticExperimentType.CANVAS_SQUARE));
		staticExperiments.add(StaticExperiment.getInstanceOfStaticExperimentType(StaticExperimentType.HTML_SQUARE));
		runStaticExperiments(staticExperiments);

		List<AnimatedExperiment> animatedExperiments = new ArrayList<AnimatedExperiment>();
		animatedExperiments.add(AnimatedExperiment.getInstanceOfAnimatedExperimentType(AnimatedExperimentType.OPACITY_ALL_CANVAS));
		animatedExperiments.add(AnimatedExperiment.getInstanceOfAnimatedExperimentType(AnimatedExperimentType.OPACITY_ALL_HTML_CSS));
		animatedExperiments.add(AnimatedExperiment.getInstanceOfAnimatedExperimentType(AnimatedExperimentType.OPACITY_ALL_HTML_JAVASCRIPT));
		animatedExperiments.add(AnimatedExperiment.getInstanceOfAnimatedExperimentType(AnimatedExperimentType.OPACITY_ALL_SVG_CSS));
		animatedExperiments.add(AnimatedExperiment.getInstanceOfAnimatedExperimentType(AnimatedExperimentType.OPACITY_ALL_SVG_JAVASCRIPT));
		runAnimatedExperiments(animatedExperiments);
	}
	
	private void runStaticExperiments(List<StaticExperiment> staticExperiments) {
		experimentDate = new Date();
		for (StaticExperiment staticExperiment : staticExperiments) {
			for (Browser browser : Browser.values()) {
				staticExperiment.setBrowser(browser);
				staticExperiment.setDate(experimentDate);
				List<StaticExperimentResult> experimentResults = new ArrayList<StaticExperimentResult>();
				for (int k = 0; k <= 10000; k = k + 2000) {
					int numberOfElements = k + 1;
					StaticExperimentResult experientResult = runStaticExperimentIteration(staticExperiment, numberOfElements);
					experimentResults.add(experientResult);
				}
				staticExperiment.setExperimentResults(experimentResults);
				MongoDBConnector.getInstance().insertStaticExperiment(staticExperiment);
			}
		}
	}
	
	private void runAnimatedExperiments(List<AnimatedExperiment> animatedExperiments) {
		experimentDate = new Date();
		for (AnimatedExperiment animatedExperiment : animatedExperiments) {
			for (Browser browser : Browser.values()) {
				animatedExperiment.setBrowser(browser);
				animatedExperiment.setDate(experimentDate);
				List<AnimatedExperimentResult> experimentResults = new ArrayList<AnimatedExperimentResult>();
				for (int k = 0; k <= 2000; k = k + 200) {
					if (k == 0) {
						k = 1;
					}
					int numberOfElements = k + 3;
					AnimatedExperimentResult experientResult = runAnimatedExperimentIteration(animatedExperiment, numberOfElements);
					experimentResults.add(experientResult);
				}
				animatedExperiment.setExperimentResults(experimentResults);
				MongoDBConnector.getInstance().insertAnimatedExperiment(animatedExperiment);
			}
		}
	}
	
	private StaticExperimentResult runStaticExperimentIteration(StaticExperiment staticExperiment, int numberOfElements) {
		StaticExperimentResult result = null;
		final WebDriver webDriver = staticExperiment.getBrowser().getWebDriver();
		webDriver.manage().window().maximize();
		webDriver.get(staticExperiment.getExperiementPath()+numberOfElements);
		WebDriverWait wait = new WebDriverWait(webDriver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("button")));
		String videoFilePath = "ScreenRecordings/STATIC-"+getNameFromStaticExperiment(staticExperiment) + numberOfElements + ".mkv";
		ScreenRecordingThread screenRecordingThread = new ScreenRecordingThread(staticExperiment.getRecordingTimeInSeconds(), videoFilePath);
		screenRecordingThread.start();
		try {
			Thread.sleep(1000);
			Actions builder  = new Actions(webDriver);
			WebElement button = webDriver.findElement(By.id("button"));
			builder.moveToElement(button).clickAndHold().build().perform();
			builder.release(button).build().perform();
			System.out.println("Waiting for screen recording process to finish");
			screenRecordingThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		webDriver.quit();
		String framesDirectoryPath = "Frames/FRAMES-" + getNameFromStaticExperiment(staticExperiment) + numberOfElements;
		FramesExtractionThread framesExtractionRunnable = new FramesExtractionThread(videoFilePath, framesDirectoryPath);
		Thread framesExtractionThread = new Thread(framesExtractionRunnable);
		framesExtractionThread.run();
		System.out.println("Waiting for frames extraction process to finish");
		try {
			framesExtractionThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		File[] frames = framesExtractionRunnable.getFrames();
		String croppedFramesFolder = "Frames/FRAMES-" + getNameFromStaticExperiment(staticExperiment) + numberOfElements+"-CROPPED";
		VideoCropThread videoCropThread = new VideoCropThread(frames, croppedFramesFolder);
		videoCropThread.start();
		System.out.println("Waiting for frames cropping to finish");
		try {
			videoCropThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		result = getStaticExperimentResultFromFrames(videoCropThread.getCroppedFrames());
		Pair<Integer, Integer> pixelChangeInterval = getPixelChangeIntervalInFrames(result.getFramePixelDifferences());
		result.setRenderTimeInFrames(pixelChangeInterval.getRight().intValue()-pixelChangeInterval.getLeft().intValue());
		result.setNumberOfObjects(numberOfElements);
		FileUtil.deleteFilesThatMatchRegexInFolder("ScreenRecordings/", "STATIC");
		FileUtil.deleteDirectoriesThatMatchRegexInFolder("Frames/", "FRAMES");
		return result;
	}
	
	private String getNameFromStaticExperiment(StaticExperiment staticExperiment) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(staticExperiment.getWebTechnologyCombination()+"-");
		stringBuilder.append(staticExperiment.getGraphicPrimitive().name()+"-");
		stringBuilder.append(staticExperiment.getBrowser()+"-");
		for(Property styledProperty : staticExperiment.getStyleProperties()) {
			stringBuilder.append(styledProperty.name()+"-");
		}
		return stringBuilder.toString();
	}
	
	private String getNameFromAnimatedExperiment(AnimatedExperiment animatedExperiment) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(animatedExperiment.getWebTechnologyCombination()+"-");
		stringBuilder.append(animatedExperiment.getGraphicPrimitive().name()+"-");
		stringBuilder.append(animatedExperiment.getBrowser()+"-");
		for(AnimatedProperty animatedProperty : animatedExperiment.getAnimation().getProperties()) {
			stringBuilder.append(animatedProperty.name()+"-");
		}
		return stringBuilder.toString();
	}

	private AnimatedExperimentResult runAnimatedExperimentIteration(AnimatedExperiment animatedExperiment, int numberOfElements) {
		AnimatedExperimentResult result = null;
		final WebDriver webDriver = animatedExperiment.getBrowser().getWebDriver();
		webDriver.manage().window().maximize();
		webDriver.get(animatedExperiment.getExperiementPath() + numberOfElements);
		WebDriverWait wait = new WebDriverWait(webDriver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("button")));
		String videoFilePath;
		try {
			videoFilePath = "ScreenRecordings/ANIMATED-"+getNameFromAnimatedExperiment(animatedExperiment) + numberOfElements + ".mkv";
			ScreenRecordingThread screenRecordingThread = new ScreenRecordingThread(animatedExperiment.getRecordingTimeInSeconds(), videoFilePath);
			screenRecordingThread.start();
			Thread.sleep(1000);
			webDriver.findElement(By.id("button")).click();
			System.out.println("Waiting for screen recording process to finish");
			screenRecordingThread.join();
		webDriver.quit();
		String framesDirectoryPath = "Frames/FRAMES-" +getNameFromAnimatedExperiment(animatedExperiment) + numberOfElements;
		FramesExtractionThread framesExtractionRunnable = new FramesExtractionThread(videoFilePath, framesDirectoryPath);
		Thread framesExtractionThread = new Thread(framesExtractionRunnable);
		framesExtractionThread.run();
		System.out.println("Waiting for frames extraction process to finish");
		try {
			framesExtractionThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		File[] frames = framesExtractionRunnable.getFrames();
		String croppedFramesFolder = "Frames/FRAMES-" + getNameFromAnimatedExperiment(animatedExperiment) + numberOfElements+"-CROPPED";
		VideoCropThread videoCropThread = new VideoCropThread(frames, croppedFramesFolder);
		videoCropThread.start();
		System.out.println("Waiting for frames cropping to finish");
		try {
			videoCropThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		result = getAnimatedExperimentResultFromFrames(videoCropThread.getCroppedFrames());
		addIdenticalFramesNumberRateToAnimatedExperimentResults(result);
		result.setNumberOfObjects(numberOfElements-3);
		FileUtil.deleteFilesThatMatchRegexInFolder("ScreenRecordings/", "ANIMATED");
		FileUtil.deleteDirectoriesThatMatchRegexInFolder("Frames/", "FRAMES");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private AnimatedExperimentResult getAnimatedExperimentResultFromFrames(File[] frames) {
		AnimatedExperimentResult result = new AnimatedExperimentResult();
		int numberIdenticalFrames = 0;
		int numberNonIdenticalFrames = 0;
		int sumOfDifferentPixels = 0;
		Map<Integer, Integer> frameDifferenceProcess = new TreeMap<Integer, Integer>();
		for (int i = 0; i < frames.length; i++) {
			try {
				if (i != 0) {
					final File imageFileA = frames[i - 1];
					final File imageFileB = frames[i];
					BufferedImage frameA = ImageIO.read(imageFileA);
					BufferedImage frameB = ImageIO.read(imageFileB);
					int numberOfDifferentPixels = numberOfDifferentPixels(frameA, frameB);
					sumOfDifferentPixels = sumOfDifferentPixels + numberOfDifferentPixels;
					frameDifferenceProcess.put(Integer.valueOf(i), Integer.valueOf(numberOfDifferentPixels));
					if(numberOfDifferentPixels > 0) {
						numberNonIdenticalFrames++;
					} else {
						numberIdenticalFrames++;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Number Identical Frames:"+numberIdenticalFrames);
		System.out.println("Number Non Identical Frames:"+numberNonIdenticalFrames);
		System.out.println("Sum of different pixels: "+sumOfDifferentPixels);
		result.setNumberOfIdenticalFramesOverall(numberIdenticalFrames);
		result.setNumberOfNonIdenticalFramesOverall(numberNonIdenticalFrames);
		result.setSumOfDifferentPixels(sumOfDifferentPixels);
		result.setFramePixelDifferences(frameDifferenceProcess);
		return result;
	}
	
	private StaticExperimentResult getStaticExperimentResultFromFrames(File[] frames) {
		StaticExperimentResult result = new StaticExperimentResult();
		Map<Integer, Integer> frameDifferenceProcess = new TreeMap<Integer, Integer>();
		for (int i = 0; i < frames.length; i++) {
			try {
				if (i != 0) {
					final File imageFileA = frames[i - 1];
					System.out.println("Image File: "+imageFileA.getName());
					final File imageFileB = frames[i];
					BufferedImage frameA = ImageIO.read(imageFileA);
					BufferedImage frameB = ImageIO.read(imageFileB);
					int numberOfDifferentPixels = numberOfDifferentPixels(frameA, frameB);
					System.out.println("Number of different pixels: "+numberOfDifferentPixels);
					frameDifferenceProcess.put(Integer.valueOf(i), Integer.valueOf(numberOfDifferentPixels));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		result.setFramePixelDifferences(frameDifferenceProcess);
		return result;
	}
	
	private Pair<Integer, Integer> getPixelChangeIntervalInFrames(Map<Integer, Integer> framePixelDifferences) {
		Pair<Integer, Integer> result = null;
		Iterator<Entry<Integer, Integer>> iterator = framePixelDifferences.entrySet().iterator();
		int possibleFirstPixelChangePosition = 0;
		int possibleLastPixelChangePosition = 0;
		while (iterator.hasNext()) {
			Map.Entry<Integer, Integer> pair = iterator.next();
			int numberOfDifferentPixels = pair.getValue().intValue();
			if (numberOfDifferentPixels > 0 && possibleFirstPixelChangePosition == 0) {
				possibleFirstPixelChangePosition = pair.getKey().intValue();
			}
			if (numberOfDifferentPixels > 0) {
				possibleLastPixelChangePosition = pair.getKey().intValue();
			}
		}
		result = new ImmutablePair<Integer, Integer>(possibleFirstPixelChangePosition, possibleLastPixelChangePosition);
		return result;
	}
	
	private void addIdenticalFramesNumberRateToAnimatedExperimentResults(AnimatedExperimentResult animatedExperimentResult) {
		Pair<Integer, Integer> animationIntervallInFrames = getPixelChangeIntervalInFrames(animatedExperimentResult.getFramePixelDifferences());
		animatedExperimentResult.setAnimationTimeInFrames(animationIntervallInFrames.getRight().intValue() - animationIntervallInFrames.getLeft().intValue());
		int intenticalFramesNumber = getIdenticalFramesInFramesRange(animatedExperimentResult, animationIntervallInFrames);
		double animationDurationInFrames = animatedExperimentResult.getAnimationTimeInFrames();
		double identicalFramesInFramesTime = intenticalFramesNumber / animationDurationInFrames;
		double identicalFrameRatePer60Frames = identicalFramesInFramesTime * 60;
		System.out.println("identicalFrameRatePer60Frames " + identicalFrameRatePer60Frames);
		animatedExperimentResult.setIdenticalFrameRatePer60Frames(identicalFrameRatePer60Frames);
	}
	
	private int getIdenticalFramesInFramesRange(AnimatedExperimentResult animatedExperimentResult, Pair<Integer, Integer> framesRange) {
		int result = 0;
		Map<Integer, Integer> framePixelDifferences = animatedExperimentResult.getFramePixelDifferences();
		for (int i = framesRange.getLeft().intValue(); i <= framesRange.getRight().intValue(); i++) {
			if(framePixelDifferences != null) {
				int numberOfDifferentPixels = framePixelDifferences.get(Integer.valueOf(i));
				if (numberOfDifferentPixels == 0)
					result++;
	        }
	    }
	    return result;
	}
	
	private int numberOfDifferentPixels(BufferedImage imageA, BufferedImage imageB) {
		int numberOfDifferentPixels = 0;
		byte[] pixelsImageA = ((DataBufferByte) imageA.getRaster().getDataBuffer()).getData();
		byte[] pixelsImageB = ((DataBufferByte) imageB.getRaster().getDataBuffer()).getData();
		try {
			for (int i = 0; i < pixelsImageA.length; i++) {
				if (pixelsImageA[i] != pixelsImageB[i]) {
					numberOfDifferentPixels++;
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
		}
		return numberOfDifferentPixels;
	}
}
