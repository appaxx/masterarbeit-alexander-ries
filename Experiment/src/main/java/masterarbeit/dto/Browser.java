package masterarbeit.dto;

import org.knowm.xchart.style.markers.Marker;
import org.knowm.xchart.style.markers.SeriesMarkers;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public enum Browser {
	CHROME(SeriesMarkers.CIRCLE),
	FIREFOX(SeriesMarkers.DIAMOND),
	OPERA(SeriesMarkers.SQUARE);
	
	private Marker marker;
	
	private Browser(Marker marker) {
		this.marker = marker;
	}

	public Marker getMarker() {
		return marker;
	}
	
	public WebDriver getWebDriver() {
		WebDriver result = null;
		switch (this) {
		case CHROME:
			System.setProperty("webdriver.chrome.driver", "/java/browser drivers/chromedriver");
			result = new ChromeDriver();
			break;
		case FIREFOX:
			result = new FirefoxDriver();
			break;
		case OPERA:
			System.setProperty("webdriver.chrome.driver", "/java/browser drivers/operadriver-4");
			result = new ChromeDriver();
			break;
		}
		return result;
	}
}
