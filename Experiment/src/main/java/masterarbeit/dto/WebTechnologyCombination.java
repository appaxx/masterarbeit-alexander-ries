package masterarbeit.dto;

import java.awt.Color;


public enum WebTechnologyCombination {
	HTML_CSS(Color.BLUE, WebTechnology.HTML, WebTechnology.CSS),
	HTML_JAVASCRIPT(Color.GREEN, WebTechnology.HTML, WebTechnology.JAVA_SCRIPT),
	CANVAS_JAVASCRIPT(Color.RED, WebTechnology.HTML_CANVAS, WebTechnology.JAVA_SCRIPT),
	SVG_JAVASCRIPT(Color.ORANGE, WebTechnology.SVG, WebTechnology.JAVA_SCRIPT),
	SVG_CSS(Color.CYAN, WebTechnology.SVG, WebTechnology.CSS);
	
	private Color color;
	private WebTechnology[] webTechnologies;
	
	private WebTechnologyCombination(Color color, WebTechnology... webTechnologies) {
		this.color = color;
		this.webTechnologies = webTechnologies;
	}
	
	public Color getColor() {
		return color;
	}

	public WebTechnology[] getWebTechnologies() {
		return webTechnologies;
	}
}
