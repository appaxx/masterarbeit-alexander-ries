package masterarbeit.dto.experiment;

public abstract class ExperimentPathConstants {
	protected static final String PROJECT_DIRECTORY = System.getProperty("user.dir");
	protected static final String FILE_PREFIX = "file://";
	protected static final String NUMBER_OF_ELEMENTS_PARAMETER = "elementsNumber";
}
