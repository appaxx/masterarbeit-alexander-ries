package masterarbeit.dto.experiment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import masterarbeit.dto.GraphicPrimitive;
import masterarbeit.dto.WebTechnologyCombination;
import masterarbeit.dto.experiment.result.StaticExperimentResult;

public class StaticExperiment extends Experiment {
	private List<StaticExperimentResult> experimentResults;
	private List<Property> styleProperties;
	
	private static final int RECORDING_TIME_IN_SECONDS = 10;
	
	protected StaticExperiment(String experimentPath, GraphicPrimitive graphicPrimitive, WebTechnologyCombination webTechnologyCombination, Property[] styleProperties) {
		super();
		this.setExperiementPath(experimentPath);
		this.setGraphicPrimitive(graphicPrimitive);
		this.setWebTechnologyCombination(webTechnologyCombination);
		List<Property> propertiesList = new ArrayList<Property>();
		if(styleProperties != null) {
			propertiesList.addAll(Arrays.asList(styleProperties));
		}
		this.setStyleProperties(propertiesList);
	}
	
	public List<StaticExperimentResult> getExperimentResults() {
		return experimentResults;
	}

	public void setExperimentResults(List<StaticExperimentResult> experimentResults) {
		this.experimentResults = experimentResults;
	}
	
	public List<Property> getStyleProperties() {
		return styleProperties;
	}

	public void setStyleProperties(List<Property> styleProperties) {
		this.styleProperties = styleProperties;
	}

	public static StaticExperiment getInstanceOfStaticExperimentType(StaticExperimentType staticExperimentType) {
		StaticExperiment result = new StaticExperiment(staticExperimentType.getStaticExperiment().getExperiementPath(), staticExperimentType.getStaticExperiment().getGraphicPrimitive(), staticExperimentType.getStaticExperiment().getWebTechnologyCombination(), staticExperimentType.getStaticExperiment().getStyleProperties().toArray(new Property[staticExperimentType.getStaticExperiment().getStyleProperties().size()]));
		return result;
	}

	@Override
	public int getRecordingTimeInSeconds() {
		return RECORDING_TIME_IN_SECONDS;
	}
}
