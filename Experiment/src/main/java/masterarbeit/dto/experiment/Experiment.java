package masterarbeit.dto.experiment;

import java.util.Date;

import org.openqa.selenium.WebDriver;

import masterarbeit.dto.Browser;
import masterarbeit.dto.GraphicPrimitive;
import masterarbeit.dto.WebTechnologyCombination;

public abstract class Experiment {
	private String experiementPath;
	private WebTechnologyCombination webTechnologyCombination;
	private GraphicPrimitive graphicPrimitive;
	private Browser browser;
	private Date date;
	protected WebDriver webDriver;
	
	public String getExperiementPath() {
		return experiementPath;
	}
	
	public void setExperiementPath(String experiementPath) {
		this.experiementPath = experiementPath;
	}
	
	public WebTechnologyCombination getWebTechnologyCombination() {
		return webTechnologyCombination;
	}
	
	public void setWebTechnologyCombination(WebTechnologyCombination webTechnologyCombination) {
		this.webTechnologyCombination = webTechnologyCombination;
	}
	
	public GraphicPrimitive getGraphicPrimitive() {
		return graphicPrimitive;
	}
	
	public void setGraphicPrimitive(GraphicPrimitive graphicPrimitive) {
		this.graphicPrimitive = graphicPrimitive;
	}
	
	public Browser getBrowser() {
		return browser;
	}

	public void setBrowser(Browser browser) {
		this.browser = browser;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public abstract int getRecordingTimeInSeconds();
}
