package masterarbeit.dto.experiment;

public enum AnimatedProperty {
	HEIGHT,
	LENGHT,
	POSITION,
	COLOR,
	OPACITY,
	SCALE;
}
