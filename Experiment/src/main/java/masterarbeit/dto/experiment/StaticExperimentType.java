package masterarbeit.dto.experiment;

import static masterarbeit.dto.experiment.ExperimentPathConstants.FILE_PREFIX;
import static masterarbeit.dto.experiment.ExperimentPathConstants.NUMBER_OF_ELEMENTS_PARAMETER;
import static masterarbeit.dto.experiment.ExperimentPathConstants.PROJECT_DIRECTORY;
import masterarbeit.dto.GraphicPrimitive;
import masterarbeit.dto.WebTechnologyCombination;

public enum StaticExperimentType {
	HTML_SQUARE(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Square/STATIC%20-%20HTML,%20Square/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.SQUARE, WebTechnologyCombination.HTML_JAVASCRIPT, null)),
	CANVAS_SQUARE(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Square/STATIC%20-%20Canvas,%20Square/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.SQUARE, WebTechnologyCombination.CANVAS_JAVASCRIPT, null)),
	SVG_SQUARE(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Square/STATIC%20-%20SVG,%20Square/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.SQUARE, WebTechnologyCombination.SVG_JAVASCRIPT, null)),
	HTML_CIRCLE(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Circle/STATIC%20-%20HTML,%20Circle/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.CIRCLE, WebTechnologyCombination.HTML_JAVASCRIPT, null)),
	CANVAS_CIRCLE(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Circle/STATIC%20-%20Canvas,%20Circle/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.CIRCLE, WebTechnologyCombination.CANVAS_JAVASCRIPT, null)),
	SVG_CIRCLE(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Circle/STATIC%20-%20SVG,%20Circle/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.CIRCLE, WebTechnologyCombination.SVG_JAVASCRIPT, null)),
	HTML_LINE(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Line/STATIC%20-%20HTML,%20Line/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.LINE, WebTechnologyCombination.HTML_JAVASCRIPT, null)),
	CANVAS_LINE(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Line/STATIC%20-%20Canvas,%20Line/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.LINE, WebTechnologyCombination.CANVAS_JAVASCRIPT, null)),
	SVG_LINE(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Line/STATIC%20-%20SVG,%20Line/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.LINE, WebTechnologyCombination.SVG_JAVASCRIPT, null)),
	
	//Shadow
	HTML_SQUARE_P_SHADOW(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Shadow/Square/STATIC%20-%20HTML,%20Square/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.SQUARE, WebTechnologyCombination.HTML_JAVASCRIPT, new Property[]{Property.SHADOW})),
	CANVAS_SQUARE_P_SHADOW(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Shadow/Square/STATIC%20-%20Canvas,%20Square/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.SQUARE, WebTechnologyCombination.CANVAS_JAVASCRIPT, new Property[]{Property.SHADOW})),
	SVG_SQUARE_P_SHADOW(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Shadow/Square/STATIC%20-%20SVG,%20Square/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.SQUARE, WebTechnologyCombination.SVG_JAVASCRIPT, new Property[]{Property.SHADOW})),
	HTML_CIRCLE_P_SHADOW(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Shadow/Circle/STATIC%20-%20HTML,%20Circle/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.CIRCLE, WebTechnologyCombination.HTML_JAVASCRIPT, new Property[]{Property.SHADOW})),
	CANVAS_CIRCLE_P_SHADOW(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Shadow/Circle/STATIC%20-%20Canvas,%20Circle/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.CIRCLE, WebTechnologyCombination.CANVAS_JAVASCRIPT, new Property[]{Property.SHADOW})),
	SVG_CIRCLE_P_SHADOW(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Shadow/Circle/STATIC%20-%20SVG,%20Circle/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.CIRCLE, WebTechnologyCombination.SVG_JAVASCRIPT, new Property[]{Property.SHADOW})),
	HTML_LINE_P_SHADOW(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Shadow/Line/STATIC%20-%20HTML,%20Line/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.LINE, WebTechnologyCombination.HTML_JAVASCRIPT, new Property[]{Property.SHADOW})),
	CANVAS_LINE_P_SHADOW(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Shadow/Line/STATIC%20-%20Canvas,%20Line/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.LINE, WebTechnologyCombination.CANVAS_JAVASCRIPT, new Property[]{Property.SHADOW})),
	SVG_LINE_P_SHADOW(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Shadow/Line/STATIC%20-%20SVG,%20Line/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.LINE, WebTechnologyCombination.SVG_JAVASCRIPT, new Property[]{Property.SHADOW})),
	
	//Opacity
	HTML_SQUARE_P_OPACITY(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Opacity/Square/STATIC%20-%20HTML,%20Square/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.SQUARE, WebTechnologyCombination.HTML_JAVASCRIPT,new Property[]{Property.OPACITY})),
	CANVAS_SQUARE_P_OPACITY(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Opacity/Square/STATIC%20-%20Canvas,%20Square/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.SQUARE, WebTechnologyCombination.CANVAS_JAVASCRIPT, new Property[]{Property.OPACITY})),
	SVG_SQUARE_P_OPACITY(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Opacity/Square/STATIC%20-%20SVG,%20Square/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.SQUARE, WebTechnologyCombination.SVG_JAVASCRIPT, new Property[]{Property.OPACITY})),
	HTML_CIRCLE_P_OPACITY(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Opacity/Circle/STATIC%20-%20HTML,%20Circle/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.CIRCLE, WebTechnologyCombination.HTML_JAVASCRIPT, new Property[]{Property.OPACITY})),
	CANVAS_CIRCLE_P_OPACITY(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Opacity/Circle/STATIC%20-%20Canvas,%20Circle/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.CIRCLE, WebTechnologyCombination.CANVAS_JAVASCRIPT, new Property[]{Property.OPACITY})),
	SVG_CIRCLE_P_OPACITY(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Opacity/Circle/STATIC%20-%20SVG,%20Circle/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.CIRCLE, WebTechnologyCombination.SVG_JAVASCRIPT, new Property[]{Property.OPACITY})),
	HTML_LINE_P_OPACITY(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Opacity/Line/STATIC%20-%20HTML,%20Line/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.LINE, WebTechnologyCombination.HTML_JAVASCRIPT, new Property[]{Property.OPACITY})),
	CANVAS_LINE_P_OPACITY(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Opacity/Line/STATIC%20-%20Canvas,%20Line/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.LINE, WebTechnologyCombination.CANVAS_JAVASCRIPT, new Property[]{Property.OPACITY})),
	SVG_LINE_P_OPACITY(new StaticExperiment(FILE_PREFIX+PROJECT_DIRECTORY+"/WebExperiments/Static/Opacity/Line/STATIC%20-%20SVG,%20Line/index.html"+"?"+NUMBER_OF_ELEMENTS_PARAMETER+"=", GraphicPrimitive.LINE, WebTechnologyCombination.SVG_JAVASCRIPT, new Property[]{Property.OPACITY}));
	
	private StaticExperiment staticExperiment;

	private StaticExperimentType(StaticExperiment staticExperiment) {
		this.staticExperiment = staticExperiment;
	}

	public StaticExperiment getStaticExperiment() {
		return staticExperiment;
	}
}
