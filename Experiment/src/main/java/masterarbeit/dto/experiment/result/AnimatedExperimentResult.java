package masterarbeit.dto.experiment.result;


public class AnimatedExperimentResult extends ExperimentResult {
	private int sumOfDifferentPixels;
	private int numberOfIdenticalFramesOverall;
	private int numberOfNonIdenticalFramesOverall;
	private int animationTimeInFrames;
	private double identicalFrameRatePer60Frames;
	
	public int getSumOfDifferentPixels() {
		return sumOfDifferentPixels;
	}

	public void setSumOfDifferentPixels(int sumOfDifferentPixels) {
		this.sumOfDifferentPixels = sumOfDifferentPixels;
	}
	
	public int getNumberOfIdenticalFramesOverall() {
		return numberOfIdenticalFramesOverall;
	}

	public void setNumberOfIdenticalFramesOverall(int numberOfIdenticalFramesOverall) {
		this.numberOfIdenticalFramesOverall = numberOfIdenticalFramesOverall;
	}

	public int getNumberOfNonIdenticalFramesOverall() {
		return numberOfNonIdenticalFramesOverall;
	}

	public void setNumberOfNonIdenticalFramesOverall(
			int numberOfNonIdenticalFramesOverall) {
		this.numberOfNonIdenticalFramesOverall = numberOfNonIdenticalFramesOverall;
	}

	public int getAnimationTimeInFrames() {
		return animationTimeInFrames;
	}

	public void setAnimationTimeInFrames(int animationTimeInFrames) {
		this.animationTimeInFrames = animationTimeInFrames;
	}

	public double getIdenticalFrameRatePer60Frames() {
		return identicalFrameRatePer60Frames;
	}

	public void setIdenticalFrameRatePer60Frames(
			double identicalFrameRatePer60Frames) {
		this.identicalFrameRatePer60Frames = identicalFrameRatePer60Frames;
	}
}
