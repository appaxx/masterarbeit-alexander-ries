package masterarbeit.dto.experiment.result;

import java.util.Map;


public abstract class ExperimentResult {
	private int numberOfObjects;
	private Map<Integer,Integer> framePixelDifferences;

	public int getNumberOfObjects() {
		return numberOfObjects;
	}

	public void setNumberOfObjects(int numberOfObjects) {
		this.numberOfObjects = numberOfObjects;
	}

	public Map<Integer, Integer> getFramePixelDifferences() {
		return framePixelDifferences;
	}

	public void setFramePixelDifferences(Map<Integer, Integer> framePixelDifferences) {
		this.framePixelDifferences = framePixelDifferences;
	}
}
