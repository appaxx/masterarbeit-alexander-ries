package masterarbeit.dto.experiment.result;

public class StaticExperimentResult extends ExperimentResult {
	private int renderTimeInFrames;

	public int getRenderTimeInFrames() {
		return renderTimeInFrames;
	}

	public void setRenderTimeInFrames(int renderTimeInFrames) {
		this.renderTimeInFrames = renderTimeInFrames;
	}
}
