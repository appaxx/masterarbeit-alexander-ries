package masterarbeit.dto.experiment;

import java.util.ArrayList;
import java.util.List;

public class Animation {
	private List<AnimatedProperty> properties;

	public Animation(AnimatedProperty... animatedProperties) {
		properties = new ArrayList<AnimatedProperty>();
		for(AnimatedProperty animatedProperty : animatedProperties) {
			properties.add(animatedProperty);
		}
	}

	public List<AnimatedProperty> getProperties() {
		return properties;
	}
	
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for (AnimatedProperty animatedProperty : properties) {
			stringBuilder.append(animatedProperty.name()+" ");
		}
		stringBuilder.append("Animation");
		return stringBuilder.toString();
	}
}
