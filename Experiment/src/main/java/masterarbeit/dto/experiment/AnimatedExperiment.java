package masterarbeit.dto.experiment;

import java.util.List;

import masterarbeit.dto.GraphicPrimitive;
import masterarbeit.dto.WebTechnologyCombination;
import masterarbeit.dto.experiment.result.AnimatedExperimentResult;

public class AnimatedExperiment extends Experiment {
	private Animation animation;
	private List<AnimatedExperimentResult> experimentResults;
	
	private static final int RECORDING_TIME_IN_SECONDS = 5;
	public static final String ANIMATION_BUTTON_ID = "button";

	protected AnimatedExperiment(String experimentPath, GraphicPrimitive graphicPrimitive, WebTechnologyCombination webTechnologyCombination, Animation animation) {
		super();
		this.setExperiementPath(experimentPath);
		this.setGraphicPrimitive(graphicPrimitive);
		this.setWebTechnologyCombination(webTechnologyCombination);
		this.setAnimation(animation);
	}

	public Animation getAnimation() {
		return animation;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
	}
	
	public List<AnimatedExperimentResult> getExperimentResults() {
		return experimentResults;
	}

	public void setExperimentResults(List<AnimatedExperimentResult> experimentResults) {
		this.experimentResults = experimentResults;
	}

	public static AnimatedExperiment getInstanceOfAnimatedExperimentType(AnimatedExperimentType animatedExperimentType) {
		AnimatedExperiment result = new AnimatedExperiment(animatedExperimentType.getAnimatedExperiment().getExperiementPath(), animatedExperimentType.getAnimatedExperiment().getGraphicPrimitive(), animatedExperimentType.getAnimatedExperiment().getWebTechnologyCombination(), animatedExperimentType.getAnimatedExperiment().getAnimation());
		return result;
	}
	
	@Override
	public int getRecordingTimeInSeconds() {
		return RECORDING_TIME_IN_SECONDS;
	}
}