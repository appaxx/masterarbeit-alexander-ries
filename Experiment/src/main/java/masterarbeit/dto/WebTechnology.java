package masterarbeit.dto;

public enum WebTechnology {
	HTML,
	CSS,
	JAVA_SCRIPT,
	SVG,
	HTML_CANVAS,
	WEB_GL;
}
