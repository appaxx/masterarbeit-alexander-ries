package masterarbeit.dto;

import java.awt.Color;

import org.knowm.xchart.style.markers.Marker;

public class XYSeriesData {
	private String name;
	private double[] xCoordinates;
	private double[] yCoordinates;
	private Color color;
	private Marker marker;
	
	public XYSeriesData(String name, double[] xCoordinates, double[] yCoordinates, Color color, Marker marker) {
		this.name = name;
		this.xCoordinates = xCoordinates;
		this.yCoordinates = yCoordinates;
		this.color = color;
		this.marker = marker;
	}
	
	public String getName() {
		return name;
	}

	public double[] getXCoordinates() {
		return xCoordinates;
	}
	
	public double[] getYCoordinates() {
		return yCoordinates;
	}
	
	public Color getColor() {
		return color;
	}

	public Marker getMarker() {
		return marker;
	}
}
