package masterarbeit.dto;

public enum GraphicPrimitive {
	SQUARE,
	CIRCLE,
	LINE,
	ALL
}